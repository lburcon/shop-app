package pl.burcon.druc.adbwholesale

import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito
import pl.burcon.druc.adbwholesale.model.database.DatabaseHelper
import pl.burcon.druc.adbwholesale.model.database.model.ShoppingCartModel

/**
 * Created by lukasz.burcon on 19.01.2018.
 */
class DatabaseHelperTest {
    private val databaseHelper = Mockito.mock(DatabaseHelper::class.java)

    @Test
    fun getShoppingDataTest() {
        //given
        val shoppingCartList: ArrayList<ShoppingCartModel>

        val dataList = ArrayList<ShoppingCartModel>()
        dataList.add(ShoppingCartModel(1, 10))
        dataList.add(ShoppingCartModel(2, 1))
        dataList.add(ShoppingCartModel(4, 11))
        Mockito.`when`(databaseHelper.getShoppingCartData()).thenReturn(dataList)

        //when
        shoppingCartList = databaseHelper.getShoppingCartData()

        //then
        Assert.assertEquals(shoppingCartList.size, dataList.size)
    }

    @Test
    fun getShoppingDataReturnNull() {
        //given
        val shoppingCartList: ArrayList<ShoppingCartModel>

        val dataList = ArrayList<ShoppingCartModel>()
        Mockito.`when`(databaseHelper.getShoppingCartData()).thenReturn(dataList)

        //when
        shoppingCartList = databaseHelper.getShoppingCartData()

        //then
        Assert.assertNotEquals(shoppingCartList, null)
    }

    @Test
    fun getRowCountCantBeMinus() {
        //given
        val value = 14
        val rowCount: Int
        val isMinus:Boolean
        Mockito.`when`(databaseHelper.getShoppingCartRowCount()).thenReturn(value)

        //when
        rowCount = databaseHelper.getShoppingCartRowCount()
        isMinus = rowCount < 0


        //then
        Assert.assertFalse(isMinus)
    }

    @Test
    fun getRowCountIsZero() {
        //given
        val value = 0
        val rowCount: Int
        val isMinus:Boolean
        Mockito.`when`(databaseHelper.getShoppingCartRowCount()).thenReturn(value)

        //when
        rowCount = databaseHelper.getShoppingCartRowCount()
        isMinus = rowCount == 0


        //then
        Assert.assertTrue(isMinus)
    }

    @Test
    fun getRowCountIsPositive() {
        //given
        val value = 14
        val rowCount: Int
        val isMinus:Boolean
        Mockito.`when`(databaseHelper.getShoppingCartRowCount()).thenReturn(value)

        //when
        rowCount = databaseHelper.getShoppingCartRowCount()
        isMinus = rowCount >= 0

        //then
        Assert.assertTrue(isMinus)
    }
}