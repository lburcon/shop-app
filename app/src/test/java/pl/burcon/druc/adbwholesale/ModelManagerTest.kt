package pl.burcon.druc.adbwholesale

import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import pl.burcon.druc.adbwholesale.model.ModelManager
import pl.burcon.druc.adbwholesale.model.database.model.ArticleModel
import pl.burcon.druc.adbwholesale.model.database.model.PictureModel

/**
 * Created by lukasz.burcon on 19.01.2018.
 */
class ModelManagerTest {

    private val modelManager = Mockito.mock(ModelManager::class.java)

    @Test
    fun getArticleByIdFromListOk() {
        //given
        val firstArticleId = 1.toLong()
        val article: ArticleModel

        `when`(modelManager.getArticleById(firstArticleId)).thenReturn(ArticleModel("nazwa nowa", 33.33, 121, "opis nowy", 3))

        //when
        article = modelManager.getArticleById(firstArticleId)

        //then
        Assert.assertNotEquals(article, null)
    }

    @Test
    fun getArticleWhichNotExist() {
        //given
        val article: ArticleModel
        val secondArticleId = 2.toLong()

        `when`(modelManager.getArticleById(secondArticleId)).thenReturn(null)

        //when
        article = modelManager.getArticleById(1)

        //then
        Assert.assertEquals(article, null)
    }

    @Test
    fun getArticleAllPictures() {
        //given
        val picturesReceived: ArrayList<PictureModel>
        val pictureList = ArrayList<PictureModel>()
        pictureList.add(PictureModel("klocek"))
        pictureList.add(PictureModel("kawałek drewna"))
        `when`(modelManager.getPictures()).thenReturn(pictureList)

        //when
        picturesReceived = modelManager.getPictures()

        //then
        Assert.assertEquals(picturesReceived.size, pictureList.size)
    }

}