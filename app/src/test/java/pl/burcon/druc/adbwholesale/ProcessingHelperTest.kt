package pl.burcon.druc.adbwholesale

import org.junit.Assert
import org.junit.Test
import pl.burcon.druc.adbwholesale.model.ProcessingHelper
import pl.burcon.druc.adbwholesale.model.database.OrderArticle
import pl.burcon.druc.adbwholesale.model.database.model.ArticleModel
import java.math.BigDecimal

/**
 * Created by lukasz.burcon on 18.01.2018.
 */

class ProcessingHelperTest {

    @Test
    fun calculateCostMoreThanZero() {
        //given
        val processingHelper = ProcessingHelper()
        val articleList = ArrayList<OrderArticle>()
        articleList.add(OrderArticle(ArticleModel("nazwa", 11.12, 2, "test", 1), 1))
        articleList.add(OrderArticle(ArticleModel("nazwa inna", 123.32, 11, "test inny", 2), 32))
        val summaryCost: BigDecimal
        val moreThanZero: Boolean

        //when
        summaryCost = processingHelper.calculateSummaryCost(articleList)
        moreThanZero = summaryCost > BigDecimal.ZERO

        //then
        Assert.assertTrue(moreThanZero)
    }


    @Test
    fun calculateCostOk() {
        //given
        val processingHelper = ProcessingHelper()
        val articleList = ArrayList<OrderArticle>()
        articleList.add(OrderArticle(ArticleModel("nazwa", 11.12, 2, "test", 1), 1))
        articleList.add(OrderArticle(ArticleModel("nazwa inna", 123.32, 11, "test inny", 2), 2))
        val summaryCost: BigDecimal
        val isCostValid:Int
        val expectedCost = BigDecimal("257.76")

        //when
        summaryCost = processingHelper.calculateSummaryCost(articleList)
        isCostValid = summaryCost.compareTo(expectedCost)

        //then
        Assert.assertSame(isCostValid, 0)
    }

    @Test
    fun calculateCostWrong() {
        //given
        val processingHelper = ProcessingHelper()
        val articleList = ArrayList<OrderArticle>()
        articleList.add(OrderArticle(ArticleModel("nazwa", 11.12, 2, "test", 1), 1))
        articleList.add(OrderArticle(ArticleModel("nazwa inna", 123.32, 11, "test inny", 2), 32))
        val summaryCost: BigDecimal
        val isCostValid:Boolean
        val expectedCost = BigDecimal(321)

        //when
        summaryCost = processingHelper.calculateSummaryCost(articleList)
        isCostValid = summaryCost == expectedCost

        //then
        Assert.assertFalse(isCostValid)
    }

    @Test
    fun countPriceNetOk() {
        //given
        val processingHelper = ProcessingHelper()
        val priceGross = 12.52
        val summaryCost: Double
        val isCostValid:Boolean
        val expectedCost = 10.178861788617885

        //when
        summaryCost = processingHelper.countPriceNet(priceGross)
        isCostValid = summaryCost.compareTo(expectedCost) == 0

        //then
        Assert.assertTrue(isCostValid)
    }

    @Test
    fun countPriceNetWrong() {
        //given
        val processingHelper = ProcessingHelper()
        val priceGross = 12.52
        val summaryCost: Double
        val isCostValid:Boolean
        val expectedCost = 11

        //when
        summaryCost = processingHelper.countPriceNet(priceGross)
        isCostValid = summaryCost.compareTo(expectedCost) == 0

        //then
        Assert.assertFalse(isCostValid)
    }

    @Test
    fun countPriceMoreThanZero() {
        //given
        val processingHelper = ProcessingHelper()
        val priceGross = 12.52
        val summaryCost: Double
        val isCostValid:Boolean

        //when
        summaryCost = processingHelper.countPriceNet(priceGross)
        isCostValid = summaryCost > 0

        //then
        Assert.assertTrue(isCostValid)
    }
}