package pl.burcon.druc.adbwholesale

import org.junit.Assert
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import pl.burcon.druc.adbwholesale.model.database.model.ArticleModel
import pl.burcon.druc.adbwholesale.presenter.MainPresenter


/**
 * Created by lukasz.burcon on 19.01.2018.
 */
class MainPresenterTest {

    private val mainPresenter2 = Mockito.mock(MainPresenter::class.java)

    @Test
    fun getInvoiceRowsCountMoreOrEqualThanZero() {
        //given
        val numberZero = 0
        val moreThanZero: Boolean
        `when`(mainPresenter2.getInvoiceRowsCount()).thenReturn(43)

        //when
        moreThanZero = mainPresenter2.getInvoiceRowsCount() >= numberZero

        //then
        Assert.assertTrue(moreThanZero)
    }

    @Test
    fun getInvoiceRowsCountReturnMinus() {
        //given
        val numberZero = 0
        val moreThanZero: Boolean
        `when`(mainPresenter2.getInvoiceRowsCount()).thenReturn(-143)

        //when
        moreThanZero = mainPresenter2.getInvoiceRowsCount() >= numberZero

        //then
        Assert.assertFalse(moreThanZero)
    }

    @Test
    fun getArticlesReturnsArticles() {
        //given
        val articles = ArrayList<ArticleModel>()
        articles.add(ArticleModel("nazwa", 11.50, 12, "opis", 1))
        articles.add(ArticleModel("nazwa nowa", 33.33, 121, "opis nowy", 3))
        `when`(mainPresenter2.getArticles()).thenReturn(articles)

        //when


        //then
        Assert.assertEquals(mainPresenter2.getArticles().size, articles.size)
    }

}