package pl.burcon.druc.adbwholesale.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import pl.burcon.druc.adbwholesale.model.database.model.WorkerModel

/**
 * Created by QUEIKE on 31.12.2017.
 */
@Dao
interface WorkerDao {
//    @Query("select * from workers")
//    fun getAllWorkers() : List<WorkerModel>

//    @Query( "select * from workers where idWorker = id")
//    fun getWorker(id: Long) : WorkerModel

    @Insert
    fun insertWorker(workerModel: WorkerModel)
}