package pl.burcon.druc.adbwholesale.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import pl.burcon.druc.adbwholesale.model.database.model.ArticleModel

/**
 * Created by lukasz.burcon on 29.12.2017.
 */

@Dao
interface ArticleDao {
    @Query("select * from articles")
    fun getAllArticles(): List<ArticleModel>

    @Query("select * from articles where idArticle == :arg0")
    fun getArticle(id: Long): ArticleModel

    @Query("select quantity from articles where idArticle = :arg0")
    fun getQuantity(idArticle: Long): Int


    @Query("UPDATE articles SET quantity = :arg1  WHERE idArticle = :arg0")
    fun updateArticleQuantity(id: Long, newQuantity: Int)


    @Insert
    fun insertArticle(articleModel: ArticleModel)
}