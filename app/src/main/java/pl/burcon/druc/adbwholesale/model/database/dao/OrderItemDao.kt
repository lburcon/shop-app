package pl.burcon.druc.adbwholesale.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import pl.burcon.druc.adbwholesale.model.database.model.OrderItemModel


/**
 * Created by QUEIKE on 31.12.2017.
 */
@Dao
interface OrderItemDao {
//    @Query("select * from order_items")
//    fun getAllOrdersItems() : List<OrderItemModel>

//    @Query( "select * from ordersItems where idOrderItem = id")
//    fun getOrderItem(id: Long) : OrderItemModel
//
//    @Query( "select * from ordersItems where idOrder = id")
//    fun getOrderItems(id: Long) : List<OrderItemModel>

    @Insert
    fun insertOrderItem(orderItemModel: OrderItemModel)
}