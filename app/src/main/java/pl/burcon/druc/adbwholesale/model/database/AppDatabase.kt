package pl.burcon.druc.adbwholesale.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import pl.burcon.druc.adbwholesale.model.database.dao.*
import pl.burcon.druc.adbwholesale.model.database.model.*

/**
 * Created by lukasz.burcon on 05.01.2018.
 */
@Database(entities = arrayOf(ArticleModel::class, PictureModel::class, ShoppingCartModel::class, OrderModel::class,
        OrderItemModel::class, WorkerModel::class, AddressModel::class, ManufacturerModel::class,
        InvoiceModel::class, CustomerModel::class, ArticleDeliveryModel::class), version = 14, exportSchema = false)
/**
 * Klasa służąca generowaniu instancji bazy danych SQLite
 *
 */
abstract class AppDatabase : RoomDatabase() {
    abstract fun pictureDao(): PictureDao
    abstract fun articleDao(): ArticleDao
    abstract fun shoppingCartDao(): ShoppingCartDao
    abstract fun orderDao(): OrderDao
    abstract fun orderItemDao(): OrderItemDao
    abstract fun articleDeliveryDao(): ArticleDeliveryDao
    abstract fun customerDao(): CustomerDao
    abstract fun invoiceDao(): InvoiceDao
    abstract fun manufacturerDao(): ManufacturerDao
    abstract fun addressDao(): AddressDao
    abstract fun workerDao(): WorkerDao
}