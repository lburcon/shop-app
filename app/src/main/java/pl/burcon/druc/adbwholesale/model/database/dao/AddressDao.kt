package pl.burcon.druc.adbwholesale.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import pl.burcon.druc.adbwholesale.model.database.model.AddressModel

/**
 * Created by QUEIKE on 31.12.2017.
 */
@Dao
interface AddressDao {
    @Query("select * from addresses")
    fun getAllAddresses(): List<AddressModel>

//    @Query( "select * from addresses where idAddress = id")
//    fun getAddress(id: Long) : AddressModel

    @Insert
    fun insertAddress(addressModel: AddressModel)
}