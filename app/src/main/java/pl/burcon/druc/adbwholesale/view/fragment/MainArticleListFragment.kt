package pl.burcon.druc.adbwholesale.view.fragment

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import com.bumptech.glide.Glide
import pl.burcon.druc.adbwholesale.R
import pl.burcon.druc.adbwholesale.model.database.model.ArticleModel
import pl.burcon.druc.adbwholesale.model.database.model.PictureModel
import pl.burcon.druc.adbwholesale.presenter.MainPresenter
import pl.burcon.druc.adbwholesale.view.adapter.ArticleListAdapter

/**
 * Created by lukasz.burcon on 02.01.2018.
 */
class MainArticleListFragment : Fragment() {

    private lateinit var listView: ListView
    private lateinit var articles: ArrayList<ArticleModel>
    private lateinit var listAdapter: ArticleListAdapter
    private lateinit var mainPresenter: MainPresenter
    private var picturesList = ArrayList<PictureModel>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)

        mainPresenter = arguments!!.getParcelable("mainPresenter")

        initialize(view)
        return view
    }

    private fun initialize(view: View) {
        listView = view.findViewById(R.id.list_view_main)
        listView.onItemClickListener = AdapterView.OnItemClickListener { _: AdapterView<*>, _: View, i: Int, _: Long ->
            handleListViewClick(i)
        }

        val async = AsyncDataReceiver(this)
        async.execute()
    }

    fun getImage(imageName: String): Int {

        return context!!.resources.getIdentifier(imageName, "drawable", context!!.packageName)
    }

    private fun handleListViewClick(position: Int) {
        prepareDescriptionDialog(position)
    }

    @SuppressLint("InflateParams")
    private fun prepareDescriptionDialog(position: Int) {
        val article = articles[position]
        var picture: PictureModel? = null
        val dialogView = layoutInflater.inflate(R.layout.dialog_article_description, null)

        val image = dialogView.findViewById<ImageView>(R.id.image_article)
        val name = dialogView.findViewById<TextView>(R.id.text_article_name)
        val description = dialogView.findViewById<TextView>(R.id.text_article_description)
        name.text = article.name
        description.text = article.description

        picturesList.forEach { element ->
            if (element.id == article.idPicture)
                picture = element
        }

        Glide.with(context!!)
                .load(getImage(picture!!.picture))
                .into(image!!)

        AlertDialog.Builder(context!!)
                .setView(dialogView)
                .setCancelable(true)
                .setPositiveButton(getString(R.string.button_accept), { _: DialogInterface, i: Int -> })
                .create()
                .show()
    }

    private class AsyncDataReceiver(val fragment: MainArticleListFragment) : AsyncTask<Void, Void, ArrayList<ArticleModel>>() {
        override fun doInBackground(vararg p0: Void?): ArrayList<ArticleModel> {
            fragment.articles = fragment.mainPresenter.getArticles()
            fragment.picturesList = fragment.mainPresenter.getPictures()
            return fragment.articles
        }

        override fun onPostExecute(result: ArrayList<ArticleModel>?) {
            super.onPostExecute(result)
            fragment.listAdapter = ArticleListAdapter(fragment.context!!, R.layout.item_list_main, result!!, fragment.mainPresenter, fragment.picturesList)
            fragment.listView.adapter = fragment.listAdapter
        }
    }
}