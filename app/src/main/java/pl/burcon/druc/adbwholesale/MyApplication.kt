package pl.burcon.druc.adbwholesale

import android.app.Application
import com.facebook.stetho.Stetho

/**
 * Created by lukasz.burcon on 17.01.2018.
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }
}