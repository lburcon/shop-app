package pl.burcon.druc.adbwholesale.view

import android.arch.persistence.room.Room
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.ListView
import pl.burcon.druc.adbwholesale.R
import pl.burcon.druc.adbwholesale.model.database.AppDatabase
import pl.burcon.druc.adbwholesale.presenter.MainPresenter
import pl.burcon.druc.adbwholesale.view.fragment.InvoiceFragment
import pl.burcon.druc.adbwholesale.view.fragment.InvoiceListFragment
import pl.burcon.druc.adbwholesale.view.fragment.MainArticleListFragment
import pl.burcon.druc.adbwholesale.view.fragment.ShoppingCartFragment


class MainView : AppCompatActivity() {
    private val DB_NAME = "ADB"
    private var GET_DB_AT_START = false

    private lateinit var database: AppDatabase
    private lateinit var mainPresenter: MainPresenter

    private lateinit var toolbar: Toolbar
    private lateinit var mainArticleListFragment: MainArticleListFragment
    private lateinit var shoppingCartFragment: ShoppingCartFragment
    private lateinit var drawerToggle: ActionBarDrawerToggle
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var invoiceListFragment: InvoiceListFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_view)
        database = Room.databaseBuilder(this, AppDatabase::class.java, DB_NAME).fallbackToDestructiveMigration().build()
        mainPresenter = MainPresenter(database)

        setupToolbar()
        setupDrawer()
        initStartingFragment()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar, menu)
        return true
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        drawerToggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true
        }
        return when (item!!.itemId) {
            R.id.action_shopping_cart -> {
                downloadDataFromApi()

                handleDrawerClick(2)
                true
            }
            else ->
                super.onOptionsItemSelected(item)
        }
    }

    private fun downloadDataFromApi() {
        mainPresenter.getDataFromApi()
        val handler = Handler()
        val runnable = Runnable {
            handleDrawerClick(0)
        }
        handler.postDelayed(runnable, 5000)
    }

    private fun initStartingFragment() {
        val testing = if (GET_DB_AT_START) 3 else 1
        handleDrawerClick(testing)
    }

    private fun setupToolbar() {
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

    private fun setupDrawer() {
        drawerLayout = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawerToggle = object : ActionBarDrawerToggle(
                this, /* host Activity */
                drawerLayout, /* DrawerLayout object */
                R.string.drawer_open, /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state.  */
            override fun onDrawerClosed(view: View) {
                super.onDrawerClosed(view)
                supportActionBar!!.title = "dupa"
            }

            /** Called when a drawer has settled in a completely open state.  */
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                supportActionBar!!.title = "duuuuupa"
            }
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        drawerToggle.isDrawerIndicatorEnabled = true
        drawerLayout.addDrawerListener(drawerToggle)

        val drawerMenu = mainPresenter.getDrawerMenu()
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, drawerMenu)
        val drawerList = findViewById<ListView>(R.id.navList)
        val header = layoutInflater.inflate(R.layout.header_drawer, drawerList, false) as ViewGroup
        drawerList.addHeaderView(header)

        drawerList.adapter = adapter
        drawerList.onItemClickListener = OnItemClickListener { _: AdapterView<*>, _: View, i: Int, _: Long ->
            handleDrawerClick(i)
        }
    }

    fun handleDrawerClick(position: Int) {
        val fragmentManager = supportFragmentManager.findFragmentById(R.id.fragment_container)

        mainArticleListFragment = MainArticleListFragment()
        shoppingCartFragment = ShoppingCartFragment()
        invoiceListFragment = InvoiceListFragment()
        addPresenterToBundle(mainArticleListFragment)
        addPresenterToBundle(shoppingCartFragment)
        addPresenterToBundle(invoiceListFragment)

        when (position) {
            1 -> {
                supportFragmentManager.popBackStack()
                replaceFragment(mainArticleListFragment, R.id.fragment_container)
            }
            2 -> {
                if (fragmentManager is InvoiceListFragment)
                    supportFragmentManager.popBackStack()
                else if (fragmentManager is InvoiceFragment) {
                    supportFragmentManager.popBackStack()
                    supportFragmentManager.popBackStack()
                }
                if (fragmentManager !is ShoppingCartFragment)
                    replaceFragment(shoppingCartFragment, R.id.fragment_container)
            }
            3 -> {
                if (fragmentManager is ShoppingCartFragment)
                    supportFragmentManager.popBackStack()
                else if (fragmentManager is InvoiceFragment) {
                    supportFragmentManager.popBackStack()
                    supportFragmentManager.popBackStack()
                }
                if (fragmentManager !is InvoiceListFragment)
                    replaceFragment(invoiceListFragment, R.id.fragment_container)
            }

        }
        drawerLayout.closeDrawer(GravityCompat.START)

    }

    //finish only when on the main screen
    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            val fragmentManager = supportFragmentManager.findFragmentById(R.id.fragment_container)
            if (fragmentManager is MainArticleListFragment) {

                finish()
            } else {
                super.onBackPressed()
            }
        }
    }

    private fun addPresenterToBundle(fragment: Fragment) {
        val bundle = Bundle()
        bundle.putParcelable("mainPresenter", mainPresenter)

        fragment.arguments = bundle
    }

    private inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
        beginTransaction().func().addToBackStack("mainStack").commit()
    }

    private fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int) {
        supportFragmentManager.inTransaction { add(frameId, fragment) }
    }

    private fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
        supportFragmentManager.inTransaction { replace(frameId, fragment) }
    }

}
