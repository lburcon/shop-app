package pl.burcon.druc.adbwholesale.view.fragment

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import pl.burcon.druc.adbwholesale.R
import pl.burcon.druc.adbwholesale.model.database.DatabaseHelper
import pl.burcon.druc.adbwholesale.presenter.MainPresenter
import pl.burcon.druc.adbwholesale.view.adapter.InvoiceListAdapter

/**
 * Created by lukasz.burcon on 12.01.2018.
 */
class InvoiceListFragment : Fragment() {

    private lateinit var listView: ListView
    private lateinit var orderItems: ArrayList<DatabaseHelper.OrderAndAllOrderedItems>
    private lateinit var listAdapter: InvoiceListAdapter
    private lateinit var mainPresenter: MainPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_invoice_list, container, false)

        mainPresenter = arguments!!.getParcelable("mainPresenter")
        initialize(view)
        return view
    }

    private fun initialize(view: View) {
        listView = view.findViewById(R.id.list_view_invoice)

        val myHeader = layoutInflater.inflate(R.layout.header_invoice, listView, false) as ViewGroup
        listView.addHeaderView(myHeader, null, false)

        val async = AsyncDataReceiver(this)
        async.execute()
    }


    private class AsyncDataReceiver(val listFragment: InvoiceListFragment) : AsyncTask<Void, Void, ArrayList<DatabaseHelper.OrderAndAllOrderedItems>>() {
        override fun doInBackground(vararg p0: Void?): ArrayList<DatabaseHelper.OrderAndAllOrderedItems> {
            listFragment.orderItems = listFragment.mainPresenter.getAllItemsOfOrders()
            return listFragment.orderItems
        }

        override fun onPostExecute(result: ArrayList<DatabaseHelper.OrderAndAllOrderedItems>?) {
            super.onPostExecute(result)
            listFragment.listAdapter = InvoiceListAdapter(listFragment.context!!, R.layout.item_list_invoice, result!!,
                    listFragment.mainPresenter, listFragment)
            listFragment.listView.adapter = listFragment.listAdapter
        }
    }

}