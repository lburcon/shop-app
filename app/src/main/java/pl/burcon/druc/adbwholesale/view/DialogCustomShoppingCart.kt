package pl.burcon.druc.adbwholesale.view

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.toast
import pl.burcon.druc.adbwholesale.R
import pl.burcon.druc.adbwholesale.presenter.MainPresenter
import pl.burcon.druc.adbwholesale.view.adapter.ArticleListAdapter


/**
 * Created by lukasz.burcon on 02.01.2018.
 */
class DialogCustomShoppingCart : DialogFragment() {
    private val PLN_MARK = "zł"
    private var QUANTITY_ZERO = 0
    private var QUANTITY_ONE = 1
    //actual selected quantity
    private var quantitySelected: Int = QUANTITY_ZERO
    //available article quantity
    private var quantityAvailable = QUANTITY_ZERO
    private lateinit var quantityView: TextView
    private lateinit var pictureName: String
    private lateinit var holder: ArticleListAdapter.ViewHolder
    private lateinit var mainPresenter: MainPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dialogView = inflater.inflate(R.layout.dialog_shopping_cart
                , container, false)

        init(dialogView)

        return dialogView
    }

    private fun init(dialogView: View) {
        holder = arguments!!.getParcelable("articleHolder")
        mainPresenter = arguments!!.getParcelable("mainPresenter")
        pictureName = arguments!!.getString("pictureName")
        quantityAvailable = holder.quantityAvailable!!
        if (quantityAvailable > QUANTITY_ZERO)
            quantitySelected = QUANTITY_ONE


        initViews(dialogView)
    }

    private fun initViews(dialogView: View) {
        val imageArticle = dialogView.findViewById<ImageView>(R.id.image_article)
        val imageIncreaseQuantity = dialogView.findViewById<ImageView>(R.id.image_button_decrease_quantity)
        val imageDecreaseQuantity = dialogView.findViewById<ImageView>(R.id.image_button_increase_quantity)
        val buttonOk = dialogView.findViewById<Button>(R.id.button_accept)
        val buttonCancel = dialogView.findViewById<Button>(R.id.button_cancel)

        val nameView = dialogView.findViewById<TextView>(R.id.text_article_name)
        val priceView = dialogView.findViewById<TextView>(R.id.text_article_price)
        quantityView = dialogView.findViewById(R.id.text_article_quantity)

        val priceWithPln = holder.price
        priceView.text = priceWithPln
        nameView.text = holder.name
        //starting quantity always is zero
        quantityView.text = quantitySelected.toString()

        imageIncreaseQuantity.onClick { imageIncreaseQuantityClicked() }
        imageDecreaseQuantity.onClick { imageDecreaseQuantityClicked() }
        buttonOk.onClick { buttonOkClicked() }
        buttonCancel.onClick { buttonCancelClicked() }

        Glide.with(context!!)
                .load(getImage(pictureName))
                .into(imageArticle)

    }

    fun getImage(imageName: String): Int {

        return context!!.resources.getIdentifier(imageName, "drawable", context!!.packageName)
    }

    private fun buttonCancelClicked() {
        this.dismiss()
    }

    private fun buttonOkClicked() {
        if (quantitySelected > QUANTITY_ZERO)
            doAsync {
                mainPresenter.addToShoppingCart(holder.articleHolder!!, quantitySelected)
            }
        else
            toast(getString(R.string.toast_select_quantity))
        this.dismiss()
    }

    private fun imageIncreaseQuantityClicked() {
        if (quantityView.text.toString().toInt() < quantityAvailable)
            quantityView.text = (++quantitySelected).toString()

    }

    private fun imageDecreaseQuantityClicked() {
        if (quantityView.text.toString().toInt() > QUANTITY_ZERO)
            quantityView.text = (--quantitySelected).toString()
    }

}