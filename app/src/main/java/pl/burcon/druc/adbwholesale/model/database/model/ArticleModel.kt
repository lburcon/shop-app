package pl.burcon.druc.adbwholesale.model.database.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by lukasz.burcon on 29.12.2017.
 */

@Entity(tableName = "articles")
data class ArticleModel(
        @ColumnInfo(name = "name") var name: String = "",
        @ColumnInfo(name = "price") var price: Double = 0.0,
        @ColumnInfo(name = "quantity") var quantity: Int = 0,
        @ColumnInfo(name = "description") var description: String = "",
        @ColumnInfo(name = "idPicture") var idPicture: Long = 0) {
    @ColumnInfo(name = "idArticle")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}
