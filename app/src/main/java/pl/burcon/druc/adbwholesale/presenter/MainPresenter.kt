package pl.burcon.druc.adbwholesale.presenter

import android.os.Parcel
import android.os.Parcelable
import pl.burcon.druc.adbwholesale.model.ModelManager
import pl.burcon.druc.adbwholesale.model.database.AppDatabase
import pl.burcon.druc.adbwholesale.model.database.DatabaseHelper
import pl.burcon.druc.adbwholesale.model.database.OrderArticle
import pl.burcon.druc.adbwholesale.model.database.model.*
import java.math.BigDecimal

/**
 * Created by lukasz.burcon on 29.12.2017.
 */

/**
 * Główny konstruktor klasy MainPresenter
 *
 */
class MainPresenter() : MainPresenterToViewInterface, Parcelable {

    /**
     * Drugi konstruktor klasy MainPresenter
     *
     * @param database  główna instancja bazy danych
     * @return MainPresenter object
     */
    constructor(database: AppDatabase) : this() {
        this.database = database
        modelManager = ModelManager(database)
    }

    private lateinit var database: AppDatabase
    private lateinit var modelManager: ModelManager

    /**
     * Pobierz dane z serwera i dodaj do bazy danych
     *
     */
    override fun getDataFromApi() {
        modelManager.getDataFromApi()
    }

    /**
     * Pobiera ilość dostępnych artykułów o podanym Id
     *
     * @param articleId  id towaru
     * @return
     */
    override fun getArticleQuantityFromCart(articleId: Long): Int {
        return modelManager.getArticleQuantityFromCart(articleId)
    }

    /**
     * Pobiera ilośc faktur będących w tabeli
     *
     * @return liczba typu Int określająca ilość faktur w tabeli
     */
    override fun getInvoiceRowsCount(): Int {
        return modelManager.getInvoiceRowsCount()
    }

    /**
     * Uaktualnia pole idInvoice w modelu OrderModel dla rekordu o podanym idOrder
     *
     * @param idOrder  id towaru
     * @param idInvoice id faktury
     */
    override fun invoiceGenerated(idOrder: Long, idInvoice: Long) {
        modelManager.setNewInvoiceToOrder(idOrder, idInvoice)
    }

    /**
     * Zwraca artykuł o podanym Id z listy
     *
     * @param articleList  lista towarów
     * @param order zamówienie
     * @return artykuł o podanym Id. Null, jeśli brak
     */
    override fun getArticleByIdFromList(articleList: ArrayList<ArticleModel>, order: OrderItemModel): ArticleModel? {
        return modelManager.getArticleByIdFromList(articleList, order)
    }

    /**
     * Zwraca wszystkie elementy tabeli articles
     *
     * @return lista artykułów typu ArticleModel
     */
    override fun getArticleTableData(): ArrayList<ArticleModel> {
        return modelManager.getArticleTableData()
    }

    /**
     * Przelicza podaną cenę brutto na cenę netto Vat 23%
     *
     * @param price  cena brutto towaru
     * @return cena netto towaru
     */
    override fun countPriceNet(price: Double): Double {
        return modelManager.countPriceNet(price)
    }

    /**
     * Zwraca artykuł o podanym Id
     *
     * @param idArticle  id towaru
     * @return artykuł typu ArticleModel
     */
    override fun getArticleById(idArticle: Long): ArticleModel {
        return modelManager.getArticleById(idArticle)
    }

    /**
     * Zwraca wszystkie zamówienia z tabeli orders
     *
     * @return lista zamówień typu OrderModel
     */
    override fun getOrderData(): ArrayList<OrderModel> {
        return modelManager.getOrderData()
    }

    /**
     * Zwraca wszystkie obrazki z tabeli pictures
     *
     * @returnlista obrazków typu PictureModel
     */
    override fun getPictures(): ArrayList<PictureModel> {
        return modelManager.getPictures()
    }

    /**
     * Zwraca mapę pozycji zamówień i zamówień niczym select po IdOrder
     *
     * @return mapa pozycji zamówień i zamówień typu OrderAndAllOrderedItems
     */
    override fun getAllItemsOfOrders(): ArrayList<DatabaseHelper.OrderAndAllOrderedItems> {
        return modelManager.getAllItemsOfOrders()
    }

    /**
     * Dodaje pozycje zamówienia do tabeli order_items. Dodaje zamówienie do tabeli orders.
     * Dba również o wyliczenie nowej ilości dostępnych towarów, które zostały zakupione w zamówieniu
     *
     * @param order  zamówienie
     * @param items  lista pozycji zamówienia
     */
    override fun addOrderToDatabase(order: OrderModel, items: ArrayList<OrderItemModel>) {
        modelManager.addToOrderItemTable(items)
        modelManager.addToOrderTable(order)
        modelManager.updateArticleQuantity(items)
    }

    /**
     * Aktualizuje tabelę shopping_cart dodając nowe artykuły
     *
     * @param ordersList    lista towarów zamówienia
     */
    override fun updateShoppingCartDatabase(ordersList: ArrayList<OrderArticle>) {
        modelManager.updateShoppingCartDatabase(ordersList)
    }

    /**
     * Zwraca ilość rekordów w tabeli shopping_cart
     *
     * @return liczba typu Int, która jest ilością rekordów
     */
    override fun checkRowsCountOfShoppingCartTable(): Int {
        return modelManager.getShoppingCartRowCount()
    }

    /**
     * Usuwa tabelę shopping_cart
     *
     */
    override fun dropShoppingCartTable() {
        modelManager.dropShoppingCartTable()
    }

    /**
     * Usuwa artykuł o podanym Id z tabeli articles
     *
     * @param idArticle  id towaru
     */
    override fun deleteArticleFromCart(idArticle: Int) {
        modelManager.deleteArticleFromCart(idArticle)
    }

    /**
     * Konstruktor potrzebny do zapisywania stanu obiektu
     *
     * @param parcel
     * @return
     */
    constructor(parcel: Parcel) : this()

    /**
     * Dodaje artykuł i ilość do tabeli shopping_cart
     *
     * @param article  przedmiot koszyka
     * @param quantity  ilość
     */
    override fun addToShoppingCart(article: ArticleModel, quantity: Int) {
        modelManager.handleArticleAddedToCart(article, quantity)
    }

    /**
     * Metoda ta pozwala sprawdzić jaki artykuł jest trzymany w tabeli shopping_cart
     *
     * @param shoppingCartItem  przedmiot koszyka
     * @return artykuł odpowiadający obiektowy shoppingCartItem w tabeli shopping_cart
     */
    override fun getParsedShoppingCartItem(shoppingCartItem: ShoppingCartModel): ArticleModel {
        return modelManager.parseShoppingCart(shoppingCartItem)
    }

    /**
     * Wylicza sumę wszystkich przedmiotów w oknie koszyka
     *
     * @param list  lista towarów danego zamówienia
     * @return liczba typu BigDecimal - suma wszystkich cen za przedmioty
     */
    override fun getSummaryCost(list: ArrayList<OrderArticle>): BigDecimal {
        return modelManager.getSummaryCost(list)
    }

    /**
     * Wylicza sumę wszystkich przedmiotów w fakturze
     *
     * @param list  lista pozycji zamówień
     * @returnliczba typu BigDecimal - suma wszystkich cen za przedmioty
     */
    override fun getSummaryCostOrder(list: ArrayList<OrderItemModel>): BigDecimal {
        return modelManager.getSummaryCostOrder(list)
    }

    /**
     * Pobiera listę stringów używaną przy tworzeniu Navigation Drawer
     *
     * @return lista typu String
     */
    override fun getDrawerMenu(): ArrayList<String> {
        return modelManager.generateDrawerMenu()
    }

    /**
     * Zwraca wszystki artykuły z tabeli articles
     *
     * @return lista artykułów typu ArticleModel
     */
    override fun getArticles(): ArrayList<ArticleModel> {
        return modelManager.getArticles()
    }

    /**
     * Zwraca wszystkie rekordy tabeli shopping_cart
     *
     * @return lista rekordów typu ShoppingCartModel
     */
    override fun getShoppingCartData(): ArrayList<ShoppingCartModel> {
        return modelManager.getShoppingCartData()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {}

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MainPresenter> {
        override fun createFromParcel(parcel: Parcel): MainPresenter {
            return MainPresenter(parcel)
        }

        override fun newArray(size: Int): Array<MainPresenter?> {
            return arrayOfNulls(size)
        }
    }

}

interface MainPresenterToViewInterface {
    fun countPriceNet(price: Double): Double
    fun getArticleById(idArticle: Long): ArticleModel
    fun getOrderData(): ArrayList<OrderModel>
    fun getPictures(): ArrayList<PictureModel>
    fun getAllItemsOfOrders(): ArrayList<DatabaseHelper.OrderAndAllOrderedItems>
    fun addOrderToDatabase(order: OrderModel, items: ArrayList<OrderItemModel>)
    fun updateShoppingCartDatabase(ordersList: ArrayList<OrderArticle>)
    fun checkRowsCountOfShoppingCartTable(): Int
    fun getParsedShoppingCartItem(shoppingCartItem: ShoppingCartModel): ArticleModel
    fun deleteArticleFromCart(idArticle: Int)
    fun dropShoppingCartTable()
    fun getArticles(): ArrayList<ArticleModel>
    fun addToShoppingCart(article: ArticleModel, quantity: Int)
    fun getShoppingCartData(): ArrayList<ShoppingCartModel>
    fun getDrawerMenu(): ArrayList<String>
    fun getSummaryCostOrder(list: ArrayList<OrderItemModel>): BigDecimal
    fun getSummaryCost(list: ArrayList<OrderArticle>): BigDecimal
    fun getArticleTableData(): ArrayList<ArticleModel>
    fun getArticleByIdFromList(articleList: ArrayList<ArticleModel>, order: OrderItemModel): ArticleModel?
    fun invoiceGenerated(idOrder: Long, idInvoice: Long)
    fun getInvoiceRowsCount(): Int
    fun getArticleQuantityFromCart(articleId: Long): Int
    fun getDataFromApi()
}