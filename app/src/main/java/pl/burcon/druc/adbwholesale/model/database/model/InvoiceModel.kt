package pl.burcon.druc.adbwholesale.model.database.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by QUEIKE on 31.12.2017.
 */
@Entity(tableName = "invoices")
data class InvoiceModel(
        @ColumnInfo(name = "dateOfIssue") var dateOfIssue: String = "",
        @ColumnInfo(name = "idWorker") var idWorker: Long = 0,
        @ColumnInfo(name = "idOrder") var idOrder: Long = 0,
        @ColumnInfo(name = "idCustomer") var idCustomer: Long = 0) {
    @ColumnInfo(name = "idInvoice")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}