package pl.burcon.druc.adbwholesale.model

import pl.burcon.druc.adbwholesale.model.database.model.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by lukasz.burcon on 16.01.2018.
 */

/**
 * Główny konstruktor klasy FirebaseHelper, wykorzystywanej do łączenia się z API
 *
 */
class FirebaseHelper {
    interface AdbApi {

        /**
         * GET method, receives list of AddressModel data from REST API
         *
         * @return list of AddressModel objects
         */
        @GET("/addresses/getAddresses")
        fun getAddresses(): Call<List<AddressModel>>

        /**
         * GET method, receives list of ArticleDeliveryModel data from REST API
         *
         * @return list of ArticleDeliveryModel objects
         */
        @GET("/articlesdelivers/getArticlesDelivers")
        fun getArticleDeliveries(): Call<List<ArticleDeliveryModel>>

        /**
         * GET method, receives list of ArticleModel data from REST API
         *
         * @return list of ArticleModel objects
         */
        @GET("/articles/getArticles")
        fun getArticles(): Call<List<ArticleModel>>

        /**
         * GET method, receives list of CustomerModel data from REST API
         *
         * @return list of CustomerModel objects
         */
        @GET("/customers/getCustomers")
        fun getCustomers(): Call<List<CustomerModel>>

        /**
         * GET method, receives list of InvoiceModel data from REST API
         *
         * @return list of InvoiceModel objects
         */
        @GET("/invoices/getInvoices")
        fun getInvoices(): Call<List<InvoiceModel>>

        /**
         * GET method, receives list of ManufacturerModel data from REST API
         *
         * @return list of ManufacturerModel objects
         */
        @GET("/manufacturers/getManufacturers")
        fun getManufacturers(): Call<List<ManufacturerModel>>

        /**
         * GET method, receives list of OrderItemModel data from REST API
         *
         * @return list of OrderItemModel objects
         */
        @GET("/ordersitems/getItemsOrders")
        fun getOrderItems(): Call<List<OrderItemModel>>

        /**
         * GET method, receives list of OrderModel data from REST API
         *
         * @return list of OrderModel objects
         */
        @GET("/orders/getOrders")
        fun getOrders(): Call<List<OrderModel>>

        /**
         * GET method, receives list of PictureModel data from REST API
         *
         * @return list of PictureModel objects
         */
        @GET("/pictures/getPictures")
        fun getPictures(): Call<List<PictureModel>>

        /**
         * GET method, receives list of WorkerModel data from REST API
         *
         * @return list of WorkerModel objects
         */
        @GET("/workers/getWorkers")
        fun getWorkers(): Call<List<WorkerModel>>

        /**
         * GET method, receives list of InvoiceModel data from REST API
         *
         * @return list of InvoiceModel objects
         */
        @POST("/invoices/addInvoice")
        fun postInvoice(): Call<InvoiceModel>

        /**
         * GET method, receives list of OrderModel data from REST API
         *
         * @return list of OrderModel objects
         */
        @POST("/orders/addOrder")
        fun postOrder(): Call<OrderModel>

        /**
         * GET method, receives list of OrderItemModel data from REST API
         *
         * @return list of OrderItemModel objects
         */
        @POST("/ordersitems/addItem")
        fun postOrderItem(): Call<OrderItemModel>

    }
}