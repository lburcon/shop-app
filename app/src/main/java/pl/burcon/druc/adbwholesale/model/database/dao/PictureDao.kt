package pl.burcon.druc.adbwholesale.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import pl.burcon.druc.adbwholesale.model.database.model.PictureModel


/**
 * Created by QUEIKE on 30.12.2017.
 */
@Dao
interface PictureDao {
    @Query("select * from pictures")
    fun getAllPictures(): List<PictureModel>

    @Query("select picture from pictures where idPicture = :arg0")
    fun getPicture(id: Long): String

    @Insert
    fun insertPicture(pictureModel: PictureModel)
}