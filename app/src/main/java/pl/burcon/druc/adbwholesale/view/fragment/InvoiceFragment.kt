package pl.burcon.druc.adbwholesale.view.fragment

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import pl.burcon.druc.adbwholesale.R
import pl.burcon.druc.adbwholesale.model.database.model.ArticleModel
import pl.burcon.druc.adbwholesale.model.database.model.OrderItemModel
import pl.burcon.druc.adbwholesale.model.database.model.OrderModel
import pl.burcon.druc.adbwholesale.presenter.MainPresenter
import pl.burcon.druc.adbwholesale.view.adapter.InvoiceGeneratedListAdapter
import java.util.*

/**
 * Created by lukasz.burcon on 12.01.2018.
 */
class InvoiceFragment : Fragment() {

    private val INVOICE_NUMBER_STRING = "Invoice nr:"
    private val INVOICE_CITY_STRING = "Wrocław"
    private lateinit var listView: ListView
    private lateinit var order: OrderModel
    private lateinit var orderWithItemsArray: ArrayList<OrderItemModel>
    //whole article table
    private lateinit var articleList: ArrayList<ArticleModel>
    private lateinit var listAdapter: InvoiceGeneratedListAdapter
    private lateinit var mainPresenter: MainPresenter
    private lateinit var valueNetFooter: TextView
    private lateinit var valueVatFooter: TextView
    private lateinit var valueGrossFooter: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_invoice, container, false)

        mainPresenter = arguments!!.getParcelable("mainPresenter")
        order = arguments!!.getParcelable("order")
        orderWithItemsArray = arguments!!.getParcelableArrayList<OrderItemModel>("orderItems")

        initialize(view)
        return view
    }

    private fun initialize(view: View) {
        initListView(view)

        val invoiceNumberView = view.findViewById<TextView>(R.id.text_number_invoice)
        val invoiceCityDateView = view.findViewById<TextView>(R.id.text_place_invoice)
        val invoiceSellerView = view.findViewById<TextView>(R.id.text_seller_invoice)
        val invoiceCustomerView = view.findViewById<TextView>(R.id.text_customer_invoice)
        val invoiceWorkerView = view.findViewById<TextView>(R.id.text_worker_invoice)

        //prepare data
        val invoiceText = INVOICE_NUMBER_STRING + " " + order.orderNumber

        val dateWithoutTime = order.orderDate.split(" ")[0]
        val invoiceCityDate = INVOICE_CITY_STRING + ", " + dateWithoutTime

        //hardcoded for testing
        val invoiceSeller = "Seller \nWholesale ADB \nul. Wrocławska 32 \n50-459 Wrocław \nNIP  722-13-11-392"
        val invoiceCustomer = "Customer \nJan Kowalski \nul. Warszawska 1c \n50-321 Wrocław"
        val invoiceWorker = "Worker: \nJakub Bunbonn"


        invoiceNumberView.text = invoiceText
        invoiceCityDateView.text = invoiceCityDate
        invoiceSellerView.text = invoiceSeller
        invoiceCustomerView.text = invoiceCustomer
        invoiceCustomerView.text = invoiceCustomer
        invoiceWorkerView.text = invoiceWorker

    }

    private fun initListView(view: View) {
        listView = view.findViewById(R.id.list_view_generated_invoice)

        val header = layoutInflater.inflate(R.layout.header_generated_invoice, listView, false) as ViewGroup
        val footer = layoutInflater.inflate(R.layout.footer_generated_invoice, listView, false) as ViewGroup
        valueNetFooter = footer.findViewById(R.id.text_value_net_item_invoice)
        valueVatFooter = footer.findViewById(R.id.text_value_vat_item_invoice)
        valueGrossFooter = footer.findViewById(R.id.text_value_gross_item_invoice)

        listView.addHeaderView(header, null, false)
        listView.addFooterView(footer, null, false)

        val receiver = AsyncDataReceiver(this, mainPresenter)
        receiver.execute()
    }

    private class AsyncDataReceiver(val listFragment: InvoiceFragment, val mainPresenter: MainPresenter) : AsyncTask<Void, Void, ArrayList<ArticleModel>>() {
        override fun doInBackground(vararg p0: Void?): ArrayList<ArticleModel> {
            listFragment.articleList = listFragment.mainPresenter.getArticleTableData()
            return listFragment.articleList
        }

        override fun onPostExecute(result: ArrayList<ArticleModel>?) {
            super.onPostExecute(result)
            listFragment.listAdapter = InvoiceGeneratedListAdapter(listFragment.context!!, R.layout.item_list_generated_invoice,
                    listFragment.orderWithItemsArray, listFragment.mainPresenter, result!!)
            listFragment.listView.adapter = listFragment.listAdapter

            setupSummary(listFragment, mainPresenter)
        }

        private fun setupSummary(listFragment: InvoiceFragment, mainPresenter: MainPresenter) {

            var valueNet = 0.0
            var valueVat = 0.0
            var valueGross = 0.0
            listFragment.orderWithItemsArray.forEach { elementOrder ->
                val article = mainPresenter.getArticleByIdFromList(listFragment.articleList, elementOrder)
                val priceNet = mainPresenter.countPriceNet(article!!.price)

                valueGross += article.price * elementOrder.quantity
                valueNet += priceNet * elementOrder.quantity
                valueVat += valueGross - valueNet
            }

            listFragment.valueNetFooter.text = String.format("%.2f", valueNet)
            listFragment.valueVatFooter.text = String.format("%.2f", valueVat)
            listFragment.valueGrossFooter.text = String.format("%.2f", valueGross)

        }
    }
}