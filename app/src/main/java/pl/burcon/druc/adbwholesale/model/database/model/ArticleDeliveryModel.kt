package pl.burcon.druc.adbwholesale.model.database.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by QUEIKE on 30.12.2017.
 */

@Entity(tableName = "articles_delivers")
data class ArticleDeliveryModel(
        @ColumnInfo(name = "quantity") var quantity: Int = 0,
        @ColumnInfo(name = "idArticle") var idArticle: Long = 0,
        @ColumnInfo(name = "idManufacturer") var idManufacturer: Long = 0) {
    @ColumnInfo(name = "idArticleDelivery")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

}
