package pl.burcon.druc.adbwholesale.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import pl.burcon.druc.adbwholesale.model.database.model.InvoiceModel

/**
 * Created by QUEIKE on 31.12.2017.
 */
@Dao
interface InvoiceDao {
    @Query("select * from invoices")
    fun getAllInvoices(): List<InvoiceModel>

    @Query("select count(idInvoice) from invoices")
    fun getRowCount(): Int

    @Insert
    fun insertInvoice(invoiceModel: InvoiceModel)
}