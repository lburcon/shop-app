package pl.burcon.druc.adbwholesale.model.database

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Relation
import pl.burcon.druc.adbwholesale.model.database.model.*

/**
 * Created by lukasz.burcon on 05.01.2018.
 */

/**
 * Główny konstruktor klasy ModelManager, wykorzystywanej do działań na bazie danych
 *
 */
class DatabaseHelper(private val mDatabase: AppDatabase) {

    /**
     * Zwraca wszystkie rekordy tabeli shopping_cart
     *
     * @return lista rekordów typu ShoppingCartModel
     */
    fun getShoppingCartData(): ArrayList<ShoppingCartModel> {
        return ArrayList(mDatabase.shoppingCartDao().getAllShoppingCartItems())
    }

    /**
     * Metoda ta pozwala sprawdzić jaki artykuł jest trzymany w tabeli shopping_cart
     *
     * @param shoppingCartItem  przedmiot koszyka
     * @return artykuł odpowiadający obiektowy shoppingCartItem w tabeli shopping_cart
     */
    fun parseShoppingCart(shoppingCartModel: ShoppingCartModel): ArticleModel {
        return mDatabase.articleDao().getArticle(shoppingCartModel.idArticle)
    }

    /**
     * Zwraca wszystki artykuły z tabeli articles
     *
     * @return lista artykułów typu ArticleModel
     */
    fun getArticles(): ArrayList<ArticleModel> {

        return ArrayList(mDatabase.articleDao().getAllArticles())
    }

    /**
     * Dodaje artykuł oraz jego ilośc do tabeli shopping_cart
     *
     * @param article  artykuł do dodania
     * @param quantity  ilość
     */
    fun addToCart(article: ArticleModel, quantity: Int) {
        mDatabase.shoppingCartDao().insertItemToShoppingCart(ShoppingCartModel(article.id, quantity))
    }

    /**
     * Usuwa artykuł o podanym Id z tabeli articles
     *
     * @param idArticle  id towaru
     */
    fun deleteArticleFromCart(idArticle: Int) {
        mDatabase.shoppingCartDao().deleteShoppingCartItem(idArticle.toLong())
    }

    /**
     * Usuwa tabelę shopping_cart
     *
     */
    fun dropShoppingCartTable() {
        mDatabase.shoppingCartDao().dropShoppingCartTable()
    }

    /**
     * Zwraca ilość rekordów w tabeli shopping_cart
     *
     * @return liczba typu Int, która jest ilością rekordów
     */
    fun getShoppingCartRowCount(): Int {
        return mDatabase.orderDao().getRowCount()
    }

    /**
     * Dodaje zamówienie do tabeli orders.
     *
     * @param order  zamówienie
     */
    fun addToOrderTable(order: OrderModel) {
        mDatabase.orderDao().insertOrder(order)
    }

    /**
     * Dodaje pozycje zamówienia do tabeli order_items.
     *
     * @param items  lista pozycji zamówienia
     */
    fun addToOrderItemTable(items: ArrayList<OrderItemModel>) {
        items.forEach { element ->
            mDatabase.orderItemDao().insertOrderItem(element)
        }
    }

    /**
     * Zwraca mapę pozycji zamówień i zamówień niczym select po IdOrder
     *
     * @return mapa pozycji zamówień i zamówień typu OrderAndAllOrderedItems
     */
    fun getAllItemsOfOrders(): ArrayList<DatabaseHelper.OrderAndAllOrderedItems> {
        return ArrayList(mDatabase.orderDao().getAllOrderItemsToTheOrder())

    }

    /**
     * Zwraca wszystkie obrazki z tabeli pictures
     *
     * @returnlista obrazków typu PictureModel
     */
    fun getPictures(): ArrayList<PictureModel> {
        return ArrayList(mDatabase.pictureDao().getAllPictures())
    }

    /**
     * Zwraca wszystkie zamówienia z tabeli orders
     *
     * @return lista zamówień typu OrderModel
     */
    fun getOrderData(): ArrayList<OrderModel> {
        return ArrayList(mDatabase.orderDao().getAllOrders())
    }

    /**
     * Zwraca artykuł o podanym Id
     *
     * @param idArticle  id towaru
     * @return artykuł typu ArticleModel
     */
    fun getArticleById(idArticle: Long): ArticleModel {
        return mDatabase.articleDao().getArticle(idArticle)
    }

    /**
     * Zwraca wszystkie elementy tabeli articles
     *
     * @return lista artykułów typu ArticleModel
     */
    fun getArticleTableData(): ArrayList<ArticleModel> {
        return ArrayList(mDatabase.articleDao().getAllArticles())
    }

    /**
     * Uaktualnia pole idInvoice w modelu OrderModel dla rekordu o podanym idOrder
     *
     * @param idOrder  id towaru
     * @param idInvoice id faktury
     */
    fun setNewInvoiceToOrder(idOrder: Long, idInvoice: Long) {
        mDatabase.orderDao().updateOrdersInvoiceId(idOrder, idInvoice)
    }

    /**
     * Pobiera ilośc faktur będących w tabeli invoices
     *
     * @return liczba typu Int określająca ilość faktur w tabeli
     */
    fun getInvoiceRowsCount(): Int {
        return mDatabase.invoiceDao().getRowCount()
    }

    /**
     * Pobiera ilośc artykułów znajdujących się aktualnie w koszyku dla danego Id
     *
     * @param articleId  id przedmiotu
     * @return liczba artykułów
     */
    fun getArticleQuantityFromCart(articleId: Long): Int {
        return mDatabase.shoppingCartDao().getQuantity(articleId)
    }

    /**
     * Aktualizuje tabelę articles zmieniając ilośc dostępnych przedmiotów
     *
     * @param idArticle    id artykułu do uaktualnienia
     * @param quantity    ilośc artykułów
     */
    fun updateArticleQuantity(idArticle: Long, quantity: Int) {
        mDatabase.articleDao().updateArticleQuantity(idArticle, quantity)
    }

    /**
     * Aktualizuje tabelę shopping_cart dodając nowe artykuły
     *
     * @param id    id artykułu do dodania
     * @param quantityUpdated    ilośc artykułów
     */
    fun updateCart(id: Long, quantityUpdated: Int) {
        mDatabase.shoppingCartDao().updateCart(id, quantityUpdated)
    }

    /**
     * Pobiera ilośc artykułów z tabeli articles dla danego Id
     *
     * @param idArticle  id przedmiotu
     * @return liczba artykułów
     */
    fun getArticleQuantity(idArticle: Long): Int {
        return mDatabase.articleDao().getQuantity(idArticle)
    }

    /**
     * Dodaje listę artykułów do tabeli articles
     *
     * @param articleList  lista artykułów do dodania
     */
    fun insertArticleList(articleList: List<ArticleModel>) {
        articleList.forEach { element ->
            mDatabase.articleDao().insertArticle(element)
        }
    }

    /**
     * Dodaje listę obrazków do tabeli pictures
     *
     * @param pictureList  lista obrazków do dodania
     */
    fun insertPictureList(pictureList: List<PictureModel>) {
        pictureList.forEach { element ->
            mDatabase.pictureDao().insertPicture(element)
        }
    }

    /**
     * Dodaje listę zamówień do tabeli orders
     *
     * @param orderList  lista zamówień do dodania
     */
    fun insertOrderList(orderList: List<OrderModel>) {
        orderList.forEach { element ->
            mDatabase.orderDao().insertOrder(element)
        }
    }

    /**
     * Dodaje listę faktur do tabeli invoices
     *
     * @param list  lista faktur do dodania
     */
    fun insertInvoiceList(list: List<InvoiceModel>) {
        list.forEach { element ->
            mDatabase.invoiceDao().insertInvoice(element)
        }
    }

    /**
     * Dodaje listę pozycji zamówień do tabeli order_items
     *
     * @param list  lista pozycji zamówień do dodania
     */
    fun insertOrderItemList(list: List<OrderItemModel>) {
        list.forEach { element ->
            mDatabase.orderItemDao().insertOrderItem(element)
        }
    }

    /**
     * Dodaje listę adresów do tabeli addresses
     *
     * @param list  lista adresów do dodania
     */
    fun insertAddressList(list: List<AddressModel>) {
        list.forEach { element ->
            mDatabase.addressDao().insertAddress(element)
        }
    }

    /**
     * Dodaje listę dostaw produktu do tabeli articles_delivers
     *
     * @param list  lista dostaw do dodania
     */
    fun insertArticleDeliveryList(list: List<ArticleDeliveryModel>) {
        list.forEach { element ->
            mDatabase.articleDeliveryDao().insertArticleDelivery(element)
        }
    }

    /**
     * Dodaje listę pracowników do tabeli workers
     *
     * @param list  lista pracowników do dodania
     */
    fun insertWorkerList(list: List<WorkerModel>) {
        list.forEach { element ->
            mDatabase.workerDao().insertWorker(element)
        }
    }

    /**
     * Dodaje listę producentów do tabeli manufacturers
     *
     * @param list  lista producentów do dodania
     */
    fun insertManufacturerList(list: List<ManufacturerModel>) {
        list.forEach { element ->
            mDatabase.manufacturerDao().insertManufacturer(element)
        }
    }

    /**
     * Dodaje listę klientów do tabeli clients
     *
     * @param list  lista klientów do dodania
     */
    fun insertCustomerList(list: List<CustomerModel>) {
        list.forEach { element ->
            mDatabase.customerDao().insertCustomer(element)
        }
    }


    class OrderAndAllOrderedItems {
        @Embedded
        var order: OrderModel? = null

        @Relation(parentColumn = "idOrder", entityColumn = "idOrder")
        var orderedItems: List<OrderItemModel> = ArrayList()
    }

}

class OrderArticle(val articleModel: ArticleModel, var quantity: Int)