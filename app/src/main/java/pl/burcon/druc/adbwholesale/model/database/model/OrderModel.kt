package pl.burcon.druc.adbwholesale.model.database.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

/**
 * Created by QUEIKE on 31.12.2017.
 */
@Entity(tableName = "orders")
data class OrderModel(
        @ColumnInfo(name = "orderDate") var orderDate: String = "",
        @ColumnInfo(name = "orderNumber") var orderNumber: String = "",
        @ColumnInfo(name = "receiptDate") var receiptDate: String? = "",
        @ColumnInfo(name = "delivery") var delivery: String = "",
        @ColumnInfo(name = "paymentDate") var paymentDate: String? = "",
        @ColumnInfo(name = "idCustomer") var idCustomer: Long = 0,
        @ColumnInfo(name = "idWorker") var idWorker: Long = 0,
        @ColumnInfo(name = "idInvoice") var idInvoice: Long = 0,
        @ColumnInfo(name = "isDeferredPayment") var isDeferredPayment: Boolean = false) : Parcelable {
    @ColumnInfo(name = "idOrder")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readLong(),
            parcel.readLong(),
            parcel.readLong(),
            parcel.readByte() != 0.toByte()) {
        id = parcel.readLong()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(orderDate)
        parcel.writeString(orderNumber)
        parcel.writeString(receiptDate)
        parcel.writeString(delivery)
        parcel.writeString(paymentDate)
        parcel.writeLong(idCustomer)
        parcel.writeLong(idWorker)
        parcel.writeLong(idInvoice)
        parcel.writeByte(if (isDeferredPayment) 1 else 0)
        parcel.writeLong(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrderModel> {
        override fun createFromParcel(parcel: Parcel): OrderModel {
            return OrderModel(parcel)
        }

        override fun newArray(size: Int): Array<OrderModel?> {
            return arrayOfNulls(size)
        }
    }
}