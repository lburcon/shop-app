package pl.burcon.druc.adbwholesale.view.fragment

import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.ArrayAdapter
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.toast
import pl.burcon.druc.adbwholesale.R
import pl.burcon.druc.adbwholesale.model.database.OrderArticle
import pl.burcon.druc.adbwholesale.model.database.model.OrderItemModel
import pl.burcon.druc.adbwholesale.model.database.model.OrderModel
import pl.burcon.druc.adbwholesale.model.database.model.PictureModel
import pl.burcon.druc.adbwholesale.presenter.MainPresenter
import pl.burcon.druc.adbwholesale.view.MainView
import pl.burcon.druc.adbwholesale.view.adapter.ShoppingCartAdapter
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by lukasz.burcon on 30.12.2017.
 */
class ShoppingCartFragment : Fragment(), AdapterView.OnItemSelectedListener {

    private val PLN_MARK = "zł"
    private lateinit var listView: ListView
    private lateinit var listAdapter: ShoppingCartAdapter
    private lateinit var deliveryOption: String
    private lateinit var mainPresenter: MainPresenter
    private var ordersList = ArrayList<OrderArticle>()
    private var picturesList = ArrayList<PictureModel>()

    private lateinit var summaryCashView: TextView
    private lateinit var spinnerDelivery: Spinner
    private lateinit var buttonAccept: Button
    private lateinit var buttonDecline: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_shopping_cart, container, false)

        init(view)

        return view
    }

    override fun onStop() {
        mainPresenter.updateShoppingCartDatabase(ordersList)
        super.onStop()
    }

    private fun init(view: View) {
        mainPresenter = arguments!!.getParcelable("mainPresenter")

        listView = view.findViewById(R.id.list_view_shopping_cart)
        summaryCashView = view.findViewById(R.id.text_summary_cash)
        spinnerDelivery = view.findViewById(R.id.spinner_delivery)
        buttonAccept = view.findViewById(R.id.button_accept)
        buttonDecline = view.findViewById(R.id.button_decline)
        setupSpinner()

        setupOnClicks()
        initListView()
    }

    private fun setupSpinner() {
        val adapter = ArrayAdapter.createFromResource(context,
                R.array.planets_array, android.R.layout.simple_spinner_item)

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinnerDelivery.adapter = adapter
    }

    private fun setupOnClicks() {
        spinnerDelivery.onItemSelectedListener = this

        buttonAccept.onClick {
            if (ordersList.size > 0) {
                prepareOrder()
                toast(getString(R.string.buy_successful))
            } else
                toast(getString(R.string.buy_empty_order))
        }

        buttonDecline.onClick {
            if (ordersList.size > 0) {
                clearShoppingCart()
                toast(getString(R.string.cancel_order_successful))

            } else
                activity!!.supportFragmentManager.popBackStack()

        }
    }

    private fun prepareOrder() {
        val listOfItems = ordersList
        val listOfOrderItems = ArrayList<OrderItemModel>()

        doAsync {
            val rowsCount = mainPresenter.checkRowsCountOfShoppingCartTable().toLong()
            listOfItems.forEach { element ->
                val article = element.articleModel
                //add only items with quantity > 0
                if (element.quantity > 0)
                //add +1 to rows count to match idOrder
                    listOfOrderItems.add(OrderItemModel(rowsCount + 1, article.id, element.quantity))
            }
            Log.d("shopping cart items", listOfOrderItems.toString())

            val formattedDate = getCurrentDateTime()
            val order = OrderModel(formattedDate, ("00" + (rowsCount + 1).toString()), formattedDate, deliveryOption, formattedDate, 1, 1, 0, false)

            mainPresenter.addOrderToDatabase(order, listOfOrderItems)

            //after everything is added clear shopping cart table
            clearShoppingCart()

        }
    }

    private fun getCurrentDateTime(): String {
        val date = Calendar.getInstance().time
        val dateFormatted = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        return dateFormatted.format(date)
    }

    private fun clearShoppingCart() {
        doAsync { mainPresenter.dropShoppingCartTable() }
        activity!!.supportFragmentManager.popBackStack()
        (activity as MainView).handleDrawerClick(0)
    }


    private fun updateSummaryView() {
        val price = mainPresenter.getSummaryCost(ordersList).toDouble()
        val priceFormatted = String.format("%.2f", price) + PLN_MARK
        summaryCashView.text = priceFormatted

    }

    private fun initListView() {
        val async = AsyncDataReceiver(this)
        async.execute()
    }

    fun validateViews() {
        listAdapter.notifyDataSetChanged()
        updateSummaryView()
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        val item = p0!!.getItemAtPosition(p2).toString()
        deliveryOption = item
    }

    private class AsyncDataReceiver(val fragment: ShoppingCartFragment) : AsyncTask<Void, Void, ArrayList<OrderArticle>>() {
        override fun doInBackground(vararg p0: Void?): ArrayList<OrderArticle> {
            val orders = fragment.mainPresenter.getShoppingCartData()
            fragment.picturesList = fragment.mainPresenter.getPictures()
            orders.forEach { element ->
                fragment.ordersList.add(OrderArticle(fragment.mainPresenter.getParsedShoppingCartItem(element), element.quantity))
            }
            return fragment.ordersList
        }

        override fun onPostExecute(result: ArrayList<OrderArticle>?) {
            super.onPostExecute(result)
            fragment.listAdapter = ShoppingCartAdapter(fragment.context!!, R.layout.item_list_shopping_cart, result!!, fragment, fragment.mainPresenter, fragment.picturesList)
            fragment.listView.adapter = fragment.listAdapter
            fragment.updateSummaryView()
        }
    }
}