package pl.burcon.druc.adbwholesale.model

import pl.burcon.druc.adbwholesale.model.database.OrderArticle
import pl.burcon.druc.adbwholesale.model.database.model.ArticleModel
import pl.burcon.druc.adbwholesale.model.database.model.OrderItemModel
import java.math.BigDecimal

/**
 * Created by lukasz.burcon on 05.01.2018.
 */

/**
 * Główny konstruktor klasy ProcessingHelper, służącej do przetwarzania skomplikowanych obliczeń
 *
 */
class ProcessingHelper {
    private val PERCENTAGE_VALUE_VAT = 0.23

    /**
     * Metoda zwraca sumę wartości wszystkich towarów mnożoną przez ich ilość.
     *
     * @param list lista dostaw towarów
     * @return całkowita wartość podanych dostaw towarów
     */
    fun calculateSummaryCost(list: ArrayList<OrderArticle>): BigDecimal {
        var cost = BigDecimal.ZERO
        list.forEach { el ->
            val quantity = BigDecimal.valueOf(el.quantity.toDouble())
            val price = BigDecimal.valueOf(el.articleModel.price)
            cost += price * quantity
        }
        return cost
    }


    /**
     * Metoda Wylicza sumę wszystkich przedmiotów w fakturze. Zwraca sumę wartości wszystkich towarów mnożoną przez ich ilość.
     *
     * @param listOrderItem  lista pozycji zamówień
     * @param listArticle    lista towar
     * @return całkowita wartość podanych dostaw towarów
     */
    fun calculateSummaryCostOrder(listOrderItem: ArrayList<OrderItemModel>, listArticle: ArrayList<ArticleModel>): BigDecimal {
        var cost = BigDecimal.ZERO
        listOrderItem.forEach { orderItem ->
            val quantity = BigDecimal.valueOf(orderItem.quantity.toDouble())

            listArticle.forEach { article ->
                if (orderItem.idArticle == article.id) {
                    val price = BigDecimal.valueOf(article.price)
                    cost += price * quantity
                }
            }
        }
        return cost
    }

    /**
     * Wylicz cenę netto danej ceny brutto
     *
     * @param price  cena brutto towaru
     * @return cena netto typu Double
     */
    fun countPriceNet(price: Double): Double {
        return BigDecimal(price / (1 + PERCENTAGE_VALUE_VAT)).toDouble()
    }
}