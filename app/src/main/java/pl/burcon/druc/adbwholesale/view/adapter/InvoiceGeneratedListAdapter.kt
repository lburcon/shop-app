package pl.burcon.druc.adbwholesale.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import pl.burcon.druc.adbwholesale.R
import pl.burcon.druc.adbwholesale.model.database.model.ArticleModel
import pl.burcon.druc.adbwholesale.model.database.model.OrderItemModel
import pl.burcon.druc.adbwholesale.presenter.MainPresenter

/**
 * Created by lukasz.burcon on 12.01.2018.
 */
class InvoiceGeneratedListAdapter(context: Context, var resource: Int, private val orderList: ArrayList<OrderItemModel>,
                                  private val mainPresenter: MainPresenter, private val articleList: ArrayList<ArticleModel>) :
        ArrayAdapter<OrderItemModel>(context, resource, orderList) {

    private var layoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private lateinit var order: OrderItemModel


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val holder: ViewHolder
        val retView: View
        order = orderList[position]

        if (convertView == null) {
            retView = layoutInflater.inflate(resource, null)
            holder = ViewHolder()

            holder.numberView = retView.findViewById(R.id.text_number_item_invoice)
            holder.nameView = retView.findViewById(R.id.text_name_item_invoice)
            holder.quantityView = retView.findViewById(R.id.text_quantity_item_invoice)
            holder.priceNetView = retView.findViewById(R.id.text_price_net_item_invoice)
            holder.valueNetView = retView.findViewById(R.id.text_value_net_item_invoice)
            holder.vatView = retView.findViewById(R.id.text_vat_item_invoice)
            holder.valueVatView = retView.findViewById(R.id.text_value_vat_item_invoice)
            holder.valueGrossView = retView.findViewById(R.id.text_value_gross_item_invoice)


            retView.tag = holder

        } else {
            holder = convertView.tag as ViewHolder
            retView = convertView
        }

        val article = mainPresenter.getArticleByIdFromList(articleList, order)

        val valueGross = article!!.price * order.quantity
        val priceNet = mainPresenter.countPriceNet(article.price)
        val valueNet = priceNet * order.quantity
        val valueVat = valueGross - valueNet

        holder.numberView!!.text = (position + 1).toString()
        holder.nameView!!.text = article.name
        holder.quantityView!!.text = order.quantity.toString()
        holder.priceNetView!!.text = String.format("%.2f", priceNet)
        holder.valueNetView!!.text = String.format("%.2f", valueNet)
        holder.vatView!!.text = context.getString(R.string.vat_value)
        holder.valueVatView!!.text = String.format("%.2f", valueVat)
        holder.valueGrossView!!.text = String.format("%.2f", valueGross)


        return retView
    }


}

class ViewHolder {
    var numberView: TextView? = null
    var nameView: TextView? = null
    var quantityView: TextView? = null
    var priceNetView: TextView? = null
    var valueNetView: TextView? = null
    var vatView: TextView? = null
    var valueVatView: TextView? = null
    var valueGrossView: TextView? = null
}