package pl.burcon.druc.adbwholesale.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import pl.burcon.druc.adbwholesale.model.database.model.CustomerModel

/**
 * Created by QUEIKE on 31.12.2017.
 */
@Dao
interface CustomerDao {
//    @Query("select * from customers")
//    fun getAllCustomers() : List<CustomerModel>

//    @Query( "select * from customers where idCustomer = id")
//    fun getCustomer(id: Long) : CustomerModel

    @Insert
    fun insertCustomer(articleModel: CustomerModel)
}