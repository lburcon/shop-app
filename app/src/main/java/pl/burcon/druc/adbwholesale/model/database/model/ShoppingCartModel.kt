package pl.burcon.druc.adbwholesale.model.database.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by lukasz.burcon on 09.01.2018.
 */
@Entity(tableName = "shopping_cart")
data class ShoppingCartModel(
        @ColumnInfo(name = "idArticle") var idArticle: Long = 0,
        @ColumnInfo(name = "quantity") var quantity: Int = 0) {
    @ColumnInfo(name = "idShoppingCartEntry")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}