package pl.burcon.druc.adbwholesale.view.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.sdk25.coroutines.onClick
import pl.burcon.druc.adbwholesale.R
import pl.burcon.druc.adbwholesale.model.database.DatabaseHelper
import pl.burcon.druc.adbwholesale.model.database.model.OrderItemModel
import pl.burcon.druc.adbwholesale.model.database.model.OrderModel
import pl.burcon.druc.adbwholesale.presenter.MainPresenter
import pl.burcon.druc.adbwholesale.view.fragment.InvoiceFragment
import pl.burcon.druc.adbwholesale.view.fragment.InvoiceListFragment

/**
 * Created by lukasz.burcon on 12.01.2018.
 */
class InvoiceListAdapter(context: Context, var resource: Int, private val ordersAndItemsList: ArrayList<DatabaseHelper.OrderAndAllOrderedItems>,
                         private val mainPresenter: MainPresenter, val fragment: InvoiceListFragment) :
        ArrayAdapter<DatabaseHelper.OrderAndAllOrderedItems>(context, resource, ordersAndItemsList) {

    private var layoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private lateinit var order: OrderModel
    private lateinit var orderItems: ArrayList<OrderItemModel>
    private lateinit var invoiceSpinnerOption: String


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val holder: ViewHolder
        val retView: View
        order = ordersAndItemsList[position].order!!
        orderItems = ArrayList(ordersAndItemsList[position].orderedItems)


        if (convertView == null) {
            retView = layoutInflater.inflate(resource, null)
            holder = ViewHolder()

            holder.numberView = retView.findViewById(R.id.text_number_invoice)
            holder.dateView = retView.findViewById(R.id.text_date_invoice)
            holder.priceView = retView.findViewById(R.id.text_price_invoice)
            holder.buttonView = retView.findViewById(R.id.button_invoice)

            retView.tag = holder

        } else {
            holder = convertView.tag as ViewHolder
            retView = convertView
        }

        holder.numberView!!.text = order.orderNumber
        holder.dateView!!.text = order.orderDate


        holder.order = order

        doAsync {
            val price = mainPresenter.getSummaryCostOrder(orderItems)
            holder.priceView!!.text = String.format("%.2f", price)
        }

        setupListOnClicks(holder, position)

        return retView
    }


    private fun setupListOnClicks(holder: ViewHolder, position: Int) {
        holder.buttonView!!.onClick {
            handleButtonClick(position, holder)
        }

    }


    private fun handleButtonClick(position: Int, holder: ViewHolder) {
        val fragmentManager = fragment.activity!!.supportFragmentManager

        val fragment = InvoiceFragment()
        val bundle = Bundle()
        bundle.putParcelable("mainPresenter", mainPresenter)
        bundle.putParcelable("order", holder.order)
        bundle.putParcelableArrayList("orderItems", ArrayList(ordersAndItemsList[position].orderedItems))
        fragment.arguments = bundle
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack("mainStack")
                .commit()

        doAsync {
            val newInvoiceId = mainPresenter.getInvoiceRowsCount() + 1
            mainPresenter.invoiceGenerated(holder.order!!.id, newInvoiceId.toLong())
        }
    }

    class ViewHolder {
        var numberView: TextView? = null
        var dateView: TextView? = null
        var priceView: TextView? = null
        var buttonView: Button? = null

        var price: String? = null
        var order: OrderModel? = null


    }

}