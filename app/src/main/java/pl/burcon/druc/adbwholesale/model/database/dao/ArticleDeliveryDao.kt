package pl.burcon.druc.adbwholesale.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import pl.burcon.druc.adbwholesale.model.database.model.ArticleDeliveryModel

/**
 * Created by QUEIKE on 30.12.2017.
 */
@Dao
interface ArticleDeliveryDao {
//    @Query("select * from articles_delivers")
//    fun getAllArticlesDelivers() : List<ArticleDeliveryModel>

//    @Query( "select * from articlesDelivers where idDelivery = id")
//    fun getArticleDelivery(idDelivery: Long) : ArticleDeliveryModel

    @Insert
    fun insertArticleDelivery(articleDeliveryModel: ArticleDeliveryModel)
}