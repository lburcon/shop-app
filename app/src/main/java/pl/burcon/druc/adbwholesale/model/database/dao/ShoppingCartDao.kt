package pl.burcon.druc.adbwholesale.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import pl.burcon.druc.adbwholesale.model.database.model.ShoppingCartModel

/**
 * Created by lukasz.burcon on 09.01.2018.
 */
@Dao
interface ShoppingCartDao {
    @Query("select * from shopping_cart")
    fun getAllShoppingCartItems(): List<ShoppingCartModel>

    @Query("select quantity from shopping_cart where idArticle = :arg0")
    fun getQuantity(idArticle: Long): Int

    @Query("delete from shopping_cart where idArticle = :arg0")
    fun deleteShoppingCartItem(id: Long)

    @Query("delete from shopping_cart")
    fun dropShoppingCartTable()

    @Query("UPDATE shopping_cart SET quantity = :arg1  WHERE idArticle = :arg0")
    fun updateCart(id: Long, newQuantity: Int)

    @Insert
    fun insertItemToShoppingCart(item: ShoppingCartModel)
}