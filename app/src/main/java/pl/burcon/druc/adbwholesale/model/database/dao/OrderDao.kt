package pl.burcon.druc.adbwholesale.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import pl.burcon.druc.adbwholesale.model.database.DatabaseHelper
import pl.burcon.druc.adbwholesale.model.database.model.OrderModel

/**
 * Created by QUEIKE on 31.12.2017.
 */
@Dao
interface OrderDao {
    @Query("select * from orders")
    fun getAllOrders(): List<OrderModel>

    @Query("select * from orders where idInvoice = 0")
    fun getAllOrderItemsToTheOrder(): List<DatabaseHelper.OrderAndAllOrderedItems>

    @Query("select count(idOrder) from orders")
    fun getRowCount(): Int

    @Query("UPDATE orders SET idInvoice = :arg1  WHERE idOrder = :arg0")
    fun updateOrdersInvoiceId(idOrder: Long, newIdInvoice: Long)

    @Insert
    fun insertOrder(orderModel: OrderModel)
}