package pl.burcon.druc.adbwholesale.model.database.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

/**
 * Created by QUEIKE on 31.12.2017.
 */
@Entity(tableName = "order_items")
data class OrderItemModel(
        @ColumnInfo(name = "idOrder") var idOrder: Long = 0,
        @ColumnInfo(name = "idArticle") var idArticle: Long = 0,
        @ColumnInfo(name = "quantity") var quantity: Int = 0) : Parcelable {
    @ColumnInfo(name = "idOrderItem")
    @PrimaryKey(autoGenerate = true)
    var idOrderQuantity: Long = 0

    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readLong(),
            parcel.readInt()) {
        idOrderQuantity = parcel.readLong()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(idOrder)
        parcel.writeLong(idArticle)
        parcel.writeInt(quantity)
        parcel.writeLong(idOrderQuantity)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OrderItemModel> {
        override fun createFromParcel(parcel: Parcel): OrderItemModel {
            return OrderItemModel(parcel)
        }

        override fun newArray(size: Int): Array<OrderItemModel?> {
            return arrayOfNulls(size)
        }
    }


}