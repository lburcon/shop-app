package pl.burcon.druc.adbwholesale.view.adapter

import android.content.Context
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import org.jetbrains.anko.doAsync
import pl.burcon.druc.adbwholesale.R
import pl.burcon.druc.adbwholesale.model.database.model.ArticleModel
import pl.burcon.druc.adbwholesale.model.database.model.PictureModel
import pl.burcon.druc.adbwholesale.presenter.MainPresenter
import pl.burcon.druc.adbwholesale.view.DialogCustomShoppingCart


/**
 * Created by lukasz.burcon on 29.12.2017.
 */
class ArticleListAdapter(context: Context, var resource: Int, private var articlesList: ArrayList<ArticleModel>,
                         private val mainPresenter: MainPresenter, val pictures: ArrayList<PictureModel>) :
        ArrayAdapter<ArticleModel>(context, resource, articlesList) {

    private var layoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private lateinit var article: ArticleModel
    private lateinit var picture: PictureModel
    private val PLN_MARK = "zł"


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val holder: ViewHolder
        val retView: View
        article = articlesList[position]
        pictures.forEach { element ->
            if (element.id == article.idPicture)
                picture = element
        }
        if (convertView == null) {
            retView = layoutInflater.inflate(resource, null)
            holder = ViewHolder()

            holder.imageView = retView.findViewById(R.id.image_article)
            holder.nameView = retView.findViewById(R.id.text_article_name)
            holder.priceView = retView.findViewById(R.id.text_article_price)
            holder.cartIconView = retView.findViewById(R.id.image_shopping_cart)

            retView.tag = holder

        } else {
            holder = convertView.tag as ViewHolder
            retView = convertView
        }

        setupListOnClicks(holder)

        Glide.with(context!!)
                .load(getImage(picture.picture))
                .into(holder.imageView!!)

        holder.nameView!!.text = article.name
        var priceWithPln = String.format("%.2f", article.price) + PLN_MARK
        holder.priceView!!.text = priceWithPln

        holder.idArticle = article.id
        holder.picture = picture
        holder.articleHolder = article
        holder.name = article.name
        priceWithPln = String.format("%.2f", article.price) + PLN_MARK
        holder.price = priceWithPln
        holder.description = article.description
        holder.quantity = article.quantity

        calculateAvailability(holder)

        return retView
    }

    private fun calculateAvailability(holder: ViewHolder) {
        doAsync {
            val quantityFromCart = mainPresenter.getArticleQuantityFromCart(holder.idArticle!!)
            holder.quantityAvailable = holder.quantity!! - quantityFromCart
        }
    }

    fun getImage(imageName: String): Int {
        return context.resources.getIdentifier(imageName, "drawable", context.packageName)
    }

    private fun setupListOnClicks(holder: ViewHolder) {
        holder.cartIconView!!.setOnClickListener({ v ->
            Log.d("cart", "clicked")
            prepareShoppingCartDialog(holder)
        })
    }

    private fun prepareShoppingCartDialog(holder: ViewHolder) {
        val customDialog = DialogCustomShoppingCart()

        val bundle = Bundle()
        bundle.putParcelable("articleHolder", holder)
        bundle.putParcelable("mainPresenter", mainPresenter)
        bundle.putString("pictureName", holder.picture!!.picture)

        customDialog.arguments = bundle
        customDialog.isCancelable = false
        customDialog.show((context as AppCompatActivity).supportFragmentManager, "DialogShoppingCart")

    }

    class ViewHolder() : Parcelable {
        var imageView: ImageView? = null
        var nameView: TextView? = null
        var priceView: TextView? = null
        var cartIconView: ImageView? = null


        var articleHolder: ArticleModel? = null
        var quantity: Int? = null
        var quantityAvailable: Int? = null
        var name: String? = null
        var price: String? = null
        var description: String? = null
        var picture: PictureModel? = null
        var idArticle: Long? = null

        constructor(parcel: Parcel) : this() {
            quantity = parcel.readValue(Int::class.java.classLoader) as? Int
            quantityAvailable = parcel.readValue(Int::class.java.classLoader) as? Int
            name = parcel.readString()
            price = parcel.readString()
            description = parcel.readString()
            idArticle = parcel.readValue(Long::class.java.classLoader) as? Long
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeValue(quantity)
            parcel.writeValue(quantityAvailable)
            parcel.writeString(name)
            parcel.writeString(price)
            parcel.writeString(description)
            parcel.writeValue(idArticle)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<ViewHolder> {
            override fun createFromParcel(parcel: Parcel): ViewHolder {
                return ViewHolder(parcel)
            }

            override fun newArray(size: Int): Array<ViewHolder?> {
                return arrayOfNulls(size)
            }
        }


    }

}