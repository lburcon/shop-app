package pl.burcon.druc.adbwholesale.model.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import pl.burcon.druc.adbwholesale.model.database.model.ManufacturerModel

/**
 * Created by QUEIKE on 30.12.2017.
 */
@Dao
interface ManufacturerDao {
//    @Query("select * from manufacturers")
//    fun getAllManufacturers() : List<ManufacturerModel>

//    @Query( "select * from manufacturers where idManufacturer = id")
//    fun getManufacturer(id: Long) : ManufacturerModel

    @Insert
    fun insertManufacturer(manufacturerModel: ManufacturerModel)
}