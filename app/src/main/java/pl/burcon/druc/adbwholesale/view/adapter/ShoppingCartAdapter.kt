package pl.burcon.druc.adbwholesale.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import org.jetbrains.anko.doAsync
import pl.burcon.druc.adbwholesale.R
import pl.burcon.druc.adbwholesale.model.database.OrderArticle
import pl.burcon.druc.adbwholesale.model.database.model.ArticleModel
import pl.burcon.druc.adbwholesale.model.database.model.PictureModel
import pl.burcon.druc.adbwholesale.presenter.MainPresenter
import pl.burcon.druc.adbwholesale.view.fragment.ShoppingCartFragment


/**
 * Created by lukasz.burcon on 03.01.2018.
 */
class ShoppingCartAdapter(context: Context, var resource: Int, var orders: ArrayList<OrderArticle>, val fragment: ShoppingCartFragment,
                          val mainPresenter: MainPresenter, val pictures: ArrayList<PictureModel>) :
        ArrayAdapter<OrderArticle>(context, resource, orders) {

    private var layoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private lateinit var article: ArticleModel
    private lateinit var order: OrderArticle
    private lateinit var picture: PictureModel
    private val PLN_MARK = "zł"


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val holder: ViewHolder
        val retView: View
        order = orders[position]
        article = order.articleModel
        pictures.forEach { element ->
            if (element.id == article.idPicture)
                picture = element
        }

        if (convertView == null) {
            retView = layoutInflater.inflate(resource, null)
            holder = ViewHolder()

            holder.imageView = retView.findViewById(R.id.image_article)
            holder.nameView = retView.findViewById(R.id.text_article_name)
            holder.priceUnitView = retView.findViewById(R.id.text_article_price_unit)
            holder.priceSummaryView = retView.findViewById(R.id.text_article_price_summary)
            holder.quantityView = retView.findViewById(R.id.text_article_quantity)
            holder.trashIconView = retView.findViewById(R.id.image_article_trash)
            holder.decreaseQuantityView = retView.findViewById(R.id.image_quantity_decrease)
            holder.increaseQuantityView = retView.findViewById(R.id.image_quantity_increase)


            retView.tag = holder

        } else {
            holder = convertView.tag as ViewHolder
            retView = convertView
        }

        Glide.with(context!!)
                .load(getImage(picture.picture))
                .into(holder.imageView!!)


        setupListOnClicks(holder, position)

        holder.quantitySelected = order.quantity
        holder.name = article.name
        holder.priceUnit = article.price
        holder.quantityAvailable = article.quantity

        holder.nameView!!.text = article.name
        val priceWithPln = String.format("%.2f", article.price) + PLN_MARK
        holder.priceUnitView!!.text = priceWithPln
        holder.quantityView!!.text = holder.quantitySelected.toString()
        val price = holder.quantitySelected.toString().toInt() * holder.priceUnit.toString().toDouble()
        val priceFormatted = String.format("%.2f", price) + PLN_MARK
        holder.priceSummaryView!!.text = priceFormatted


        return retView
    }

    fun getImage(imageName: String): Int {

        return context.resources.getIdentifier(imageName, "drawable", context.packageName)
    }

    private fun setupListOnClicks(holder: ViewHolder, position: Int) {
        holder.trashIconView!!.setOnClickListener({ v ->
            Log.d("cart", "clicked")
            trashIconClicked(position)
        })
        holder.decreaseQuantityView!!.setOnClickListener({ v ->
            Log.d("dec", "clicked")
            decreaseQuantityClicked(holder, position)
        })
        holder.increaseQuantityView!!.setOnClickListener({ v ->
            Log.d("inc", "clicked")
            increaseQuantityClicked(holder, position)
        })
    }

    private fun trashIconClicked(position: Int) {
        val article = orders[position].articleModel
        doAsync {
            mainPresenter.deleteArticleFromCart(article.id.toInt())
        }
        orders.removeAt(position)
        fragment.validateViews()
    }

    private fun increaseQuantityClicked(holder: ViewHolder, position: Int) {
        if (holder.quantityView!!.text.toString().toInt() < holder.quantityAvailable) {
            holder.quantityView!!.text = (++(holder.quantitySelected)).toString()
            updateOrderQuantity(true, position)
        }
    }

    private fun decreaseQuantityClicked(holder: ViewHolder, position: Int) {
        if (holder.quantityView!!.text.toString().toInt() > 0) {
            holder.quantityView!!.text = (--(holder.quantitySelected)).toString()
            updateOrderQuantity(false, position)
        }
    }

    private fun updateOrderQuantity(increase: Boolean, position: Int) {
        val order = orders[position]
        if (increase)
            order.quantity++
        else
            order.quantity--

        orders[position] = order
        fragment.validateViews()
    }

    class ViewHolder {
        var imageView: ImageView? = null
        var nameView: TextView? = null
        var priceUnitView: TextView? = null
        var priceSummaryView: TextView? = null
        var quantityView: TextView? = null
        var trashIconView: ImageView? = null
        var decreaseQuantityView: ImageView? = null
        var increaseQuantityView: ImageView? = null


        var quantityAvailable: Int = 0
        var quantitySelected: Int = 0
        var name: String? = null
        var priceUnit: Double? = null

    }
}