package pl.burcon.druc.adbwholesale.model.database.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by QUEIKE on 31.12.2017.
 */
@Entity(tableName = "addresses")
data class AddressModel(
        @ColumnInfo(name = "street") var street: String = "",
        @ColumnInfo(name = "streetNumber") var streetNumber: Int = 0,
        @ColumnInfo(name = "apartmentNumber") var apartmenrNumber: Int = 0,
        @ColumnInfo(name = "postalCode") var postalCode: String = "",
        @ColumnInfo(name = "city") var city: String = "",
        @ColumnInfo(name = "country") var country: String = "") {
    @ColumnInfo(name = "idAddress")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}