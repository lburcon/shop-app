package pl.burcon.druc.adbwholesale.model.database.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by QUEIKE on 30.12.2017.
 */

@Entity(tableName = "pictures")
data class PictureModel(
        @ColumnInfo(name = "picture") var picture: String = "") {
    @ColumnInfo(name = "idPicture")
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}