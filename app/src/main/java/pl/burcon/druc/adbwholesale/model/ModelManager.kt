package pl.burcon.druc.adbwholesale.model

import android.util.Log
import org.jetbrains.anko.doAsync
import pl.burcon.druc.adbwholesale.model.database.AppDatabase
import pl.burcon.druc.adbwholesale.model.database.DatabaseHelper
import pl.burcon.druc.adbwholesale.model.database.OrderArticle
import pl.burcon.druc.adbwholesale.model.database.model.*
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.math.BigDecimal

/**
 * Created by lukasz.burcon on 30.12.2017.
 */

/**
 * Główny konstruktor klasy ModelManager, która zarządza logiką biznesową
 *
 */
class ModelManager(private val mDatabase: AppDatabase) : ModelManagerInterface {
    private var adbApi: FirebaseHelper.AdbApi
    private var processingHelper = ProcessingHelper()
    private var databaseHelper = DatabaseHelper(mDatabase)

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://adb-sys.herokuapp.com")
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

        adbApi = retrofit.create(FirebaseHelper.AdbApi::class.java)
    }

    /**
     * Pobierz dane z serwera i dodaj do bazy danych
     *
     */
    override fun getDataFromApi() {
        doAsync {
            val getCallArticle = adbApi.getArticles()
            val responseArticle = getCallArticle.execute()

            val dataListArticle = responseArticle.body()!!.map {
                ArticleModel(it.name, it.price, 100, it.description, it.idPicture)
            }
            databaseHelper.insertArticleList(dataListArticle)

            val getCallPicture = adbApi.getPictures()
            val responsePicture = getCallPicture.execute()

            val dataListPicture = responsePicture.body()!!.map {
                PictureModel(it.picture)
            }
            databaseHelper.insertPictureList(dataListPicture)

            val getCallOrder = adbApi.getOrders()
            val responseOrder = getCallOrder.execute()

            val dataListOrder = responseOrder.body()!!.map {
                OrderModel(it.orderDate, it.orderNumber, it.receiptDate, it.delivery, it.paymentDate,
                        it.idCustomer, it.idWorker, it.idInvoice, it.isDeferredPayment)
            }
            Log.d("response/order", dataListOrder.toString())
            databaseHelper.insertOrderList(dataListOrder)

            val getCallInvoice = adbApi.getInvoices()
            val responseInvoice = getCallInvoice.execute()

            val dataListInvoice = responseInvoice.body()!!.map {
                InvoiceModel(it.dateOfIssue, it.idWorker, it.idOrder, it.idCustomer)
            }
            databaseHelper.insertInvoiceList(dataListInvoice)

            val getCallAddress = adbApi.getAddresses()
            val responseAddress = getCallAddress.execute()

            val dataListAddress = responseAddress.body()!!.map {
                AddressModel(it.street, it.streetNumber, it.apartmenrNumber, it.postalCode, it.city, it.country)
            }
            databaseHelper.insertAddressList(dataListAddress)


            val getCallOrderItem = adbApi.getOrderItems()
            val responseOrderItem = getCallOrderItem.execute()

            val dataListOrderItem = responseOrderItem.body()!!.map {
                OrderItemModel(it.idOrder, it.idArticle, it.quantity)
            }
            databaseHelper.insertOrderItemList(dataListOrderItem)

            val getCallArticleDelivery = adbApi.getArticleDeliveries()
            val responseArticleDelivery = getCallArticleDelivery.execute()

            val dataListArticleDelivery = responseArticleDelivery.body()!!.map {
                ArticleDeliveryModel(it.quantity, it.idArticle, it.idManufacturer)
            }
            databaseHelper.insertArticleDeliveryList(dataListArticleDelivery)

            val getCallCustomer = adbApi.getCustomers()
            val responseCustomer = getCallCustomer.execute()

            val dataListCustomer = responseCustomer.body()!!.map {
                CustomerModel(it.dateOfRegistration, it.companyName, it.nip, it.forename, it.surname, it.idAddress)
            }
            databaseHelper.insertCustomerList(dataListCustomer)

            val getCallManufacturer = adbApi.getManufacturers()
            val responseManufacturer = getCallManufacturer.execute()

            val dataListManufacturer = responseManufacturer.body()!!.map {
                ManufacturerModel(it.designation)
            }
            databaseHelper.insertManufacturerList(dataListManufacturer)

            val getCallWorker = adbApi.getWorkers()
            val responseWorker = getCallWorker.execute()

            val dataListWorker = responseWorker.body()!!.map {
                WorkerModel(it.street, it.streetNumber, it.apartmenrNumber, it.postalCode, it.city, it.country)
            }
            databaseHelper.insertWorkerList(dataListWorker)
        }
    }

    /**
     * Aktualizuje tabelę shopping_cart dodając nowe artykuły
     *
     * @param ordersList    lista towarów zamówienia
     */
    override fun updateShoppingCartDatabase(ordersList: ArrayList<OrderArticle>) {
        doAsync {
            ordersList.forEach { element ->
                val article = element.articleModel
                val quantity = element.quantity

                databaseHelper.updateCart(article.id, quantity)
            }
        }
    }

    /**
     * Dodaje artykuł i ilość do tabeli shopping_cart
     *
     * @param article  przedmiot koszyka
     * @param quantity  ilość
     */
    fun handleArticleAddedToCart(article: ArticleModel, quantity: Int) {
        doAsync {
            val quantityFromCart = databaseHelper.getArticleQuantityFromCart(article.id)
            if (quantityFromCart > 0) {
                val quantityUpdated = quantityFromCart + quantity

                databaseHelper.updateCart(article.id, quantityUpdated)
            } else
                addToCart(article, quantity)
        }
    }

    /**
     * Dba również o wyliczenie nowej ilości dostępnych towarów, które zostały zakupione w zamówieniu
     *
     * @param items  lista pozycji zamówienia
     */
    override fun updateArticleQuantity(items: ArrayList<OrderItemModel>) {
        doAsync {
            items.forEach { element ->
                var quantity = databaseHelper.getArticleQuantity(element.idArticle)
                quantity -= element.quantity

                databaseHelper.updateArticleQuantity(element.idArticle, quantity)
            }
        }
    }

    /**
     * Zwraca artykuł o podanym Id z listy
     *
     * @param articleList  lista towarów
     * @param order zamówienie
     * @return artykuł o podanym Id. Null, jeśli brak
     */
    override fun getArticleByIdFromList(articleList: ArrayList<ArticleModel>, order: OrderItemModel): ArticleModel? {
        articleList.forEach { element ->
            if (order.idArticle == element.id)
                return element
        }
        return null
    }

    /**
     * Pobiera ilość dostępnych artykułów o podanym Id
     *
     * @param articleId  id towaru
     * @return
     */
    override fun getArticleQuantityFromCart(articleId: Long): Int {
        return databaseHelper.getArticleQuantityFromCart(articleId)
    }

    /**
     * Pobiera ilośc faktur będących w tabeli invoices
     *
     * @return liczba typu Int określająca ilość faktur w tabeli
     */
    override fun getInvoiceRowsCount(): Int {
        return databaseHelper.getInvoiceRowsCount()
    }

    /**
     * Uaktualnia pole idInvoice w modelu OrderModel dla rekordu o podanym idOrder
     *
     * @param idOrder  id towaru
     * @param idInvoice id faktury
     */
    override fun setNewInvoiceToOrder(idOrder: Long, idInvoice: Long) {
        databaseHelper.setNewInvoiceToOrder(idOrder, idInvoice)
    }

    /**
     * Zwraca wszystkie elementy tabeli articles
     *
     * @return lista artykułów typu ArticleModel
     */
    override fun getArticleTableData(): ArrayList<ArticleModel> {
        return databaseHelper.getArticleTableData()
    }

    /**
     * Przelicza podaną cenę brutto na cenę netto Vat 23%
     *
     * @param price  cena brutto towaru
     * @return cena netto towaru
     */
    override fun countPriceNet(price: Double): Double {
        return processingHelper.countPriceNet(price)
    }

    /**
     * Zwraca artykuł o podanym Id
     *
     * @param idArticle  id towaru
     * @return artykuł typu ArticleModel
     */
    override fun getArticleById(idArticle: Long): ArticleModel {
        return databaseHelper.getArticleById(idArticle)
    }

    /**
     * Zwraca wszystkie zamówienia z tabeli orders
     *
     * @return lista zamówień typu OrderModel
     */
    override fun getOrderData(): ArrayList<OrderModel> {
        return databaseHelper.getOrderData()
    }

    /**
     * Zwraca wszystkie obrazki z tabeli pictures
     *
     * @returnlista obrazków typu PictureModel
     */
    override fun getPictures(): ArrayList<PictureModel> {
        return databaseHelper.getPictures()
    }

    /**
     * Zwraca mapę pozycji zamówień i zamówień niczym select po IdOrder
     *
     * @return mapa pozycji zamówień i zamówień typu OrderAndAllOrderedItems
     */
    override fun getAllItemsOfOrders(): ArrayList<DatabaseHelper.OrderAndAllOrderedItems> {
        return databaseHelper.getAllItemsOfOrders()
    }

    /**
     * Dodaje pozycje zamówienia do tabeli order_items.
     *
     * @param items  lista pozycji zamówienia
     */
    override fun addToOrderItemTable(items: ArrayList<OrderItemModel>) {
        doAsync {
            databaseHelper.addToOrderItemTable(items)
        }
    }

    /**
     * Dodaje zamówienie do tabeli orders.
     *
     * @param order  zamówienie
     */
    override fun addToOrderTable(order: OrderModel) {
        doAsync {
            databaseHelper.addToOrderTable(order)
        }
    }

    /**
     * Zwraca ilość rekordów w tabeli shopping_cart
     *
     * @return liczba typu Int, która jest ilością rekordów
     */
    override fun getShoppingCartRowCount(): Int {
        return databaseHelper.getShoppingCartRowCount()
    }

    /**
     * Usuwa tabelę shopping_cart
     *
     */
    override fun dropShoppingCartTable() {
        databaseHelper.dropShoppingCartTable()
        Log.d("database/shopping_cart ", "table dropped")
    }

    /**
     * Usuwa artykuł o podanym Id z tabeli articles
     *
     * @param idArticle  id towaru
     */
    override fun deleteArticleFromCart(idArticle: Int) {
        databaseHelper.deleteArticleFromCart(idArticle)
    }

    /**
     * Wylicza sumę wszystkich przedmiotów w oknie koszyka
     *
     * @param list  lista towarów danego zamówienia
     * @return liczba typu BigDecimal - suma wszystkich cen za przedmioty
     */
    override fun getSummaryCost(list: ArrayList<OrderArticle>): BigDecimal {
        return processingHelper.calculateSummaryCost(list)
    }

    /**
     * Wylicza sumę wszystkich przedmiotów w fakturze
     *
     * @param list  lista pozycji zamówień
     * @returnliczba typu BigDecimal - suma wszystkich cen za przedmioty
     */
    override fun getSummaryCostOrder(list: ArrayList<OrderItemModel>): BigDecimal {
        val listOfArticles = ArrayList(mDatabase.articleDao().getAllArticles())
        return processingHelper.calculateSummaryCostOrder(list, listOfArticles)
    }

    /**
     * Dodaje artykuł oraz jego ilośc do tabeli shopping_cart
     *
     * @param article  artykuł do dodania
     * @param quantity  ilość
     */
    override fun addToCart(article: ArticleModel, quantity: Int) {
        doAsync {
            databaseHelper.addToCart(article, quantity)
        }
    }

    /**
     * Pobiera listę stringów używaną przy tworzeniu Navigation Drawer
     *
     * @return lista typu String
     */
    override fun generateDrawerMenu(): ArrayList<String> {
        return arrayListOf("Main page", "Shopping cart", "Invoices to be issued")
    }

    /**
     * Zwraca wszystki artykuły z tabeli articles
     *
     * @return lista artykułów typu ArticleModel
     */
    override fun getArticles(): ArrayList<ArticleModel> {
        return databaseHelper.getArticles()
    }

    /**
     * Zwraca wszystkie rekordy tabeli shopping_cart
     *
     * @return lista rekordów typu ShoppingCartModel
     */
    override fun getShoppingCartData(): ArrayList<ShoppingCartModel> {
        return databaseHelper.getShoppingCartData()
    }

    /**
     * Metoda ta pozwala sprawdzić jaki artykuł jest trzymany w tabeli shopping_cart
     *
     * @param shoppingCartItem  przedmiot koszyka
     * @return artykuł odpowiadający obiektowy shoppingCartItem w tabeli shopping_cart
     */
    override fun parseShoppingCart(shoppingCartModel: ShoppingCartModel): ArticleModel {
        return databaseHelper.parseShoppingCart(shoppingCartModel)
    }
}

interface ModelManagerInterface {
    fun countPriceNet(price: Double): Double
    fun getArticleById(idArticle: Long): ArticleModel
    fun getOrderData(): ArrayList<OrderModel>
    fun getPictures(): ArrayList<PictureModel>
    fun getAllItemsOfOrders(): ArrayList<DatabaseHelper.OrderAndAllOrderedItems>
    fun addToOrderItemTable(items: ArrayList<OrderItemModel>)
    fun addToOrderTable(order: OrderModel)
    fun updateShoppingCartDatabase(ordersList: ArrayList<OrderArticle>)
    fun getShoppingCartRowCount(): Int
    fun dropShoppingCartTable()
    fun deleteArticleFromCart(idArticle: Int)
    fun parseShoppingCart(shoppingCartModel: ShoppingCartModel): ArticleModel
    fun getArticles(): ArrayList<ArticleModel>
    fun getShoppingCartData(): ArrayList<ShoppingCartModel>
    fun addToCart(article: ArticleModel, quantity: Int)
    fun generateDrawerMenu(): ArrayList<String>
    fun getSummaryCost(list: ArrayList<OrderArticle>): BigDecimal
    fun getSummaryCostOrder(list: ArrayList<OrderItemModel>): BigDecimal
    fun getArticleTableData(): ArrayList<ArticleModel>
    fun getArticleByIdFromList(articleList: ArrayList<ArticleModel>, order: OrderItemModel): ArticleModel?
    fun setNewInvoiceToOrder(id: Long, idInvoice: Long)
    fun getInvoiceRowsCount(): Int
    fun getArticleQuantityFromCart(articleId: Long): Int
    fun updateArticleQuantity(items: ArrayList<OrderItemModel>)
    fun getDataFromApi()
}