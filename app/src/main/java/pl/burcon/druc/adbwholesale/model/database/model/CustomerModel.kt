package pl.burcon.druc.adbwholesale.model.database.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * Created by QUEIKE on 31.12.2017.
 */
@Entity(tableName = "customers")
data class CustomerModel(
        @ColumnInfo(name = "dateOfRegistration") var dateOfRegistration: String = "",
        @ColumnInfo(name = "companyName") var companyName: String? = "",
        @ColumnInfo(name = "nip") var nip: String? = "",
        @ColumnInfo(name = "forename") var forename: String? = "",
        @ColumnInfo(name = "surname") var surname: String? = "",
        @ColumnInfo(name = "idAddress") var idAddress: Long = 0) {
    @ColumnInfo(name = "idCustomer")
    @PrimaryKey(autoGenerate = true)
    var idCustomer: Long = 0

}
